$(document).ready(function() {
    var paginations = {
        limit: 4,
        offset:0
    };
    function getDataVideos(cityFilter){
//        alert(cityFilter);
            if(cityFilter == undefined){
                cityFilter = 'empty';
            }
//        alert(paginations.limit);
//        alert(paginations.offset);

        $.ajax({
            type: 'POST',
            url: '../../../../gethomevideo',
            data: {
                limit: paginations.limit,
                offset: paginations.offset,
                cityFilter:cityFilter
            },
            dataType: "json",
            success: function(response){
                //$('.videoBoxContent').html('');
                if(response.length < 4)
                    $(".show_more_video_btn").hide();
                for(var key in response){
                    $('.videoBoxContent').append("<div class='video_item prem'>"+
                        "<a class='video_link' href='../../../video/"+response[key].id+"'>"+
                            "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                            '<p class="vedeo_art">'+response[key].categoryName+'</p>'+
                            '<p class="vedeo_text">'+response[key].username+'</p>'+
                        '</a>'+
                    '</div>');
                }
            }
        });
    }
    getDataVideos($.session.get("cityTop"));
    
    $('#podbor_spec').find('input[name="name"], input[name="telefon"], input[name="e_mail"]').change(function(){
        if($(this).val() != ''){
            $(this).css('border','1px solid #d6d1ce');
        }else{
            $(this).css('border','1px solid red');
        }
    });
    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        getDataVideos($.session.get("cityTop"));
        e.preventDefault();
    });
 
    
    $(document).on('click','.podborSpecForm',function(e){
        e.preventDefault();
        var name = $('#podbor_spec').find('input[name="name"]').val();
        var e_mail = $('#podbor_spec').find('input[name="e_mail"]').val();
        var telefon = $('#podbor_spec').find('input[name="telefon"]').val();
        var category = $('#podbor_spec').find('.category_select').val();
        if((name == '') || (e_mail == '') || (telefon == '')){
            if(name == ''){
                $('#podbor_spec').find('input[name="name"]').css('border','1px solid red');
            }
            if(e_mail == ''){
                $('#podbor_spec').find('input[name="e_mail"]').css('border','1px solid red');
            }
            if(telefon == ''){
                $('#podbor_spec').find('input[name="telefon"]').css('border','1px solid red');
            }
        }else{
            console.log('send');
            $.ajax({
                type: 'POST',
                url: '../../../../sendemail',
                data: {name:name,e_mail:e_mail,telefon:telefon,category:category,},
                dataType: "json",
                success: function(response){
                    if(response.status == true){
                        swal("Форма отправлена", "", "success");
                    }else{
                        swal("Возникла ошибка", "", "warning");
                    }
                }
            });
            
        }
    });
    
    $(document).on('change','.selectCity',function(){
        if($(this).val() != ''){
            paginations = {
                limit: 4,
                offset:0
            };
            $('.videoBoxContent').html('');
            $(".show_more_video_btn").show();
            getDataVideos($(this).val());
        }
    });
    
});


