/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){

    $(document).on('click','.profession_more',function(){
        $('.profession_box ul li').each(function() {
            $(this).show();
        });
        $(this).hide();
    });

    $(document).on('click','.recomendationButtonSend',function(){
        var name = $('.recomendationSendName').val();
        var telefon = $('.recomendationSendTelefon').val();
        if((name != '') && (telefon != '')){
             $.ajax({
                type: 'POST',
                url: '../../../../recomendationvideoemail',
                data: {name:name,telefon:telefon},
                dataType: "json",
                success: function(response){
                    if(response.status == true){
                        swal("Форма отправлена", "", "success");
                    }else{
                        swal("Возникла ошибка", "", "warning");
                    }
                }
            });
        }else{
            swal('Сначала введите данные','','warning');
        }
    });
    
//    $('[data-toggle="tooltip"]').tooltip({html:true});
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        html:true
    });
    
    $(document).on('click','.pruklad',function(){
        $('input[name=input_search]').val($(this).text());
    });
    
    $(document).on('click','.loginButton',function(e){
        e.preventDefault();
        var username = $('#login-form').find('input[name="username"]').val();
        var password = $('#login-form').find('input[name="password"]').val();
        $.ajax({
            type: 'POST',
            url: '../../../../site/ajaxlogin',
            data: {username:username,password:password, rememberMe:'1'},
            dataType: "json",
            success: function(response){
                if(response.status == 'success'){
                    window.location.reload();
                }else{
                    $('#login-form').find('input[name="username"]').css('border','1px solid red');
                    $('#login-form').find('input[name="password"]').css('border','1px solid red');
                }
            }
        });
    });
    
    $(document).on('click','.signupButton',function(e){
        e.preventDefault();
        var username = $('#signup-form').find('input[name="username"]').val();
        var password = $('#signup-form').find('input[name="password"]').val();
        var confirm_password = $('#signup-form').find('input[name="confirm_password"]').val();
        var e_mail = $('#signup-form').find('input[name="e_mail"]').val();
        var access = $("input[name=access_confirmation]:checked");
        
        if((username != '') && (password != '') && (confirm_password != '') && (e_mail != '') && (access.length == 1)){
            var error = 0;
            if(username.length < 3){
                error = error + 1;
                $('#signup-form').find('input[name="username"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="username"]').css('border','1px solid #d6d1ce');
            }
            if(password.length < 3){
                error = error + 1;
                $('#signup-form').find('input[name="password"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="password"]').css('border','1px solid #d6d1ce');
            }
            if(password != confirm_password){
                error = error + 1;
                $('#signup-form').find('input[name="confirm_password"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="confirm_password"]').css('border','1px solid #d6d1ce');
            }
            
            if(error < 1){
                $.ajax({
                    type: 'POST',
                    url: '../../../../site/ajaxsignup',
                    data: {username:username,password:password, e_mail:e_mail},
                    dataType: "json",
                    success: function(response){
                        if(response.status == 'success'){
                            window.location.reload();
                        }else{
                            alert('error');
                        }
                    }
                });
            }
            
        }else{
            if(username.length < 3){
                $('#signup-form').find('input[name="username"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="username"]').css('border','1px solid #d6d1ce');
            }
            
            if(password.length < 3){
                $('#signup-form').find('input[name="password"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="password"]').css('border','1px solid #d6d1ce');
            }
            
            if(password != confirm_password){
                $('#signup-form').find('input[name="confirm_password"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="confirm_password"]').css('border','1px solid #d6d1ce');
            }
            
            if(e_mail.length < 3){
                $('#signup-form').find('input[name="e_mail"]').css('border','1px solid red');
            }else{
                $('#signup-form').find('input[name="e_mail"]').css('border','1px solid #d6d1ce');
            }
            
            if(access.length != 1){
                $('input[name=access_confirmation]').parent().find('.label_access').css('color','red');
            }else{
                $('input[name=access_confirmation]').parent().find('label').css('color','black');
            }
        }
        
    });
    
//    $('input[name=input_search]').focusout(function(){
//        $('.ul_category').hide()
//    });
    
    $(".ul_category").hover(function () {
        
    }, function () {
        setTimeout(function () {
            //$(this).next().find('.allColor').hide();
            if ($('input[name=input_search]').is(':hover')) {
            } else {
                $('.ul_category').hide()
            }
        }, 600);
    });
    
    $("input[name=input_search]").hover(function () {
        $('.ul_category').show()
    }, function () {
        setTimeout(function () {
            //$(this).next().find('.allColor').hide();
            if ($('.ul_category').is(':hover')) {
            } else {
                $('.ul_category').hide()
            }
        }, 600);
    });
    
    function dataCategorySearch(text){
        $.ajax({
            type: 'POST',
            url: '../../../../getcategoryname',
            data: {text:text},
            dataType: "json",
            success: function(response){
                $('.ul_category').html('');
                $('.ul_category').show();
                var data = response.data
                for(var key in data){
                    $('.ul_category').append('<li><a href="../../videosearch?input_search='+data[key]+'">'+data[key]+'</a></li>');
                }
            }
        });
    }
    
    $(document).on('click','.ul_category li',function(){
        $('input[name=input_search]').val($(this).text());
        $('.ul_category').hide();
    });
    
    $(document).on('keyup','input[name=input_search]',function(){
        if($(this).val().length > 2){
            var text = $(this).val();
            dataCategorySearch(text);
        }
    });
    $(document).on('click','input[name=input_search]',function(){
        if($(this).val().length > 2){
            var text = $(this).val();
            dataCategorySearch(text);
        }
    });
    
    function dataUserCity(){
        $.ajax({
            type: 'POST',
            url: '../../../../allusercity',
            data: {},
            dataType: "json",
            success: function(response){
                if(response.status == 'success'){
                    $('.selectCity').append('<option value="empty">Все города</option>');
                    
                    for(var key in response.city){
                        if($.session.get("cityTop") == response.city[key]){
                            $('.selectCity').append('<option selected="selected">'+response.city[key]+'</option>');
                        }else{
                            $('.selectCity').append('<option>'+response.city[key]+'</option>');
                        }
                    } 
//                    $('.top_select').styler({
//                        selectPlaceholder:''
//                    });
                }
            }
        });
    }
    dataUserCity();
    
    $(document).on('change','.selectCity',function(){
        if($(this).val() != ''){
            $.session.set("cityTop", $(this).val());
            //alert($.session.get("cityTop"));
            $('.cityTop option[value="'+$.session.get("cityTop")+'"]').css('border','1px solid red');
        }
    });
    
    $(function () {
        $.scrollUp({
            animation: 'fade',
            //activeOverlay: '#00FFFF',
            scrollImg: {
                active: true,
                type: 'background',
                src: '../../../img/top.png'
            }
        });
    });
    
    var stringCookieVideo = $.cookie('videos');
    var arrayVideo = stringCookieVideo.split(",");
        
    if(arrayVideo.length > 0){
        $('.countFavorite').text(arrayVideo.length - 1);
    }
});