function initialize() {

    var options = {
        types: ['(cities)'],
    //    componentRestrictions: {country: "us"}
    };

    var input = document.getElementById('myCity');
    var autocomplete = new google.maps.places.Autocomplete(input, options);
}

$(document).ready(function() {
        initialize();
        
    $('#timepicker').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
    });
    $('#timepicker_2').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
    });
    
    
    var tab_inp = $('.user_box_text_input input'); // input 
    var edit_btn = $('.edit_btn'); // btn niar input
    var save_btn = $('.save_btn'); // mobile1 btn id
    var change_btn = $('.change_btn'); // mobile2 btn conact

    function saveParametr(field_name,new_value,model_name){
        console.log(field_name);
        console.log(new_value);
        console.log(model_name);
        
        $.ajax({
            type: 'POST',
            url: '../../../../user/ajaxsavedataprofile',
            data: {field_name:field_name,new_value:new_value,model_name:model_name},
            dataType: "json",
            success: function(response){
                console.log(response);
                if(response.status == 'success'){
                    if(field_name == 'name'){
                        $('#name').text(new_value);
                    }
                    if(field_name == 'surname'){
                        $('#surname').text(new_value);
                    }
                    if(field_name == 'lastname'){
                        $('#lastname').text(new_value);
                    }
                    if(field_name == 'password'){
                        if(response.status_password == 'success'){
                            $('.editPass').show();
                            $('.confirmPass').hide();
                            $('.editPass').parent().find('.inp_pass').removeClass('act_inpt');
                            $('.editPass').parent().find('.inp_pass').attr('disabled',true);
                            $('.editPass').parent().find('.inp_pass').val('');;
                            swal("Пароль сохранен!", "", "success")
                            
                        }else{
                            alert('Ошибка');
                        }
                    }
                }
                
                if(response.status == 'address'){
                    if(response.status_address == 'error'){
                        swal("", "Такого города не существует, чтобы не возникало ошибки, выберите город из выпадающего списка, или обратитесь к Администратору сайта", "warning")
                    }
                }
            }
        });
    }

    $('.edit_btn').click(function() {
        // $(this).parent().find(tab_inp).removeAttr('disabled').addClass('act_inpt');
        if ($(this).parent().find('input[type="text"],input[type="date"],select').hasClass('act_inpt')){
            $(this).parent().find('input[type="text"],input[type="date"],select').attr('disabled','disabled').removeClass('act_inpt');
            $(this).parent().find('span').text('изменить');
            $(this).parent().find(edit_btn).removeClass('triger_btn');
            var inputValue = $(this).parent().find('input[type="text"],input[type="date"],select').val();
            var field_name = $(this).parent().find('input[type="text"],input[type="date"],select').data('field_name');
            var model_name = $(this).parent().find('input[type="text"],input[type="date"],select').data('model_name');
            var type = $(this).parent().find('input[type="text"],input[type="date"],select').data('type');
            
            if(type == 'time'){
                var inputValue1 = $(this).parent().find('#timepicker').val();
                var field_name1 = $(this).parent().find('#timepicker').data('field_name');
                var model_name1 = $(this).parent().find('#timepicker').data('model_name');
                if((inputValue1 != '') && (field_name1 != undefined)){
                    saveParametr(field_name1, inputValue1, model_name1);
                }
                
                var inputValue2 = $(this).parent().find('#timepicker_2').val();
                var field_name2 = $(this).parent().find('#timepicker_2').data('field_name');
                var model_name2 = $(this).parent().find('#timepicker_2').data('model_name');
                if((inputValue2 != '') && (field_name2 != undefined)){
                    saveParametr(field_name2, inputValue2, model_name2);
                }
            }else{
                if((inputValue != '') && (field_name != undefined)){
                    saveParametr(field_name, inputValue,model_name);
                }
            }
        } else {
            $(this).parent().find('input[type="text"],input[type="date"],select').removeAttr('disabled').addClass('act_inpt');
            $(this).parent().find('span').text('сохранить');
            $(this).parent().find(edit_btn).addClass('triger_btn');
            //$(this).parent().find('input[type="text"]').attr('value', tab_inp.val());
        }
    });

    save_btn.click(function() {
        if (tab_inp.hasClass('act_inpt')) {
            tab_inp.attr('disabled','disabled').removeClass('act_inpt');
            tab_inp.removeClass('act_inpt');
            change_btn.text('изменить');
        } else {
            tab_inp.removeAttr('disabled').addClass('act_inpt');
            change_btn.text('сохранить');
        }
    });

    change_btn.click(function() {
        if (tab_inp.hasClass('act_inpt')) {
            tab_inp.attr('disabled','disabled').removeClass('act_inpt');
            tab_inp.removeClass('act_inpt');
            change_btn.text('изменить');
        } else {
            tab_inp.removeAttr('disabled').addClass('act_inpt');
            change_btn.text('сохранить');
        }
    });
    
    
    function ajaxCategoryUser(category_id,type_request){
        $.ajax({
            type: 'POST',
            url: '../../../../user/ajaxcategoryuser',
            data: {category_id:category_id,type_request:type_request},
            dataType: "json",
            success: function(response){
                console.log(response);
            }
        });
    } 
//    
    $(document).on('click','.selectSpecialization', function(){
        var category_id = $(this).data('category_id');
        var checked = '';
        if($(this).hasClass("choisen")){
            checked = 'unchecked';
            $(this).removeClass("choisen");
        }else{
            checked = 'checked';
            $(this).addClass("choisen");
        }
        ajaxCategoryUser(category_id,checked);
    });

    $(document).on('click','.checkboxSpecialization', function(){
        if($(this).find('input[type=checkbox]').is( ":checked" )){
            var category_id = $(this).find('input[type=checkbox]').data('category_id');
            ajaxCategoryUser(category_id,'checked');
        }else{
            var category_id = $(this).find('input[type=checkbox]').data('category_id');
            ajaxCategoryUser(category_id,'unchecked');
        }
        
    });
    
    $(document).on('click','.editPass', function(){
        $(this).hide();
        $(this).parent().find('.inp_pass').addClass('act_inpt');
        $(this).parent().find('.inp_pass').attr('disabled',false);
        $('.confirmPass').show();
    });
    
    //        #58ae2c
    $(".editPass").hover(function () {
        $(this).css('color','#58ae2c');
    }, function () {
        $(this).css('color','white');
    });
    $(document).on('click','.savePass', function(){
        var newPassword = $('.changePassword').find('.newPassword').val();
        var newPasswordConfirm = $('.changePassword').find('.newPasswordConfirm').val();
        console.log(newPassword.length);
        console.log(newPasswordConfirm.length);
        if((newPassword.length > 5) && (newPasswordConfirm.length > 5)){
            if(newPassword == newPasswordConfirm){
                if((newPassword.indexOf(" ") == '-1')){
                    saveParametr('password',newPassword,'profile');
//                    field_name:'password',new_value:newPassword,model_name:'profile'
                }else{
                    swal("", "пароль не должен иметь пробелы", "warning")
                }
            }else{
                swal("", "Введенные пароли не совпадают", "warning")
            }
        }else{
            swal("", "Пароль должен быть больше 6 символов", "warning")
        }
    });
    
    
    function getCityContent(city){
            $.ajax({
                type: 'POST',
                url: '../../../../user/getcity',
                data: {city_name:city},
                dataType: "json",
                success: function(response){
                    $('.address_city_list').html('');
                    $('.address_city_list').show();
                    for(var key in response){
                        $('.address_city_list').append('<li>'+response[key]+'</li>');
                    }
                }
            });
    }
    
    $(document).on('keyup','input[name=address_city]', function(){
        var city = $(this).val();
        if(city.length > 2){
            getCityContent(city);
        }
    });
    
    $(document).on('focus','input[name=address_city]',function(){
        if($(this).val().length > 2){
            var city = $(this).val();
            getCityContent(city);
        }
    });
    
    $(document).on('click','.address_city_list li', function(){
        $('input[name=address_city]').val($(this).text());
        $('.address_city_list').hide();
    });
    
//    $(document).on('focusout','input[name=address_city]', function(){
//        $('.address_city_list').hide();
//    });
    $(".address_city_list").hover(function () {
        
    }, function () {
        setTimeout(function () {
            //$(this).next().find('.allColor').hide();
            if ($('input[name=address_city]').is(':hover')) {
            } else {
                $('.address_city_list').hide()
            }
        }, 600);
    });
    
    $("input[name=address_city]").hover(function () {
        $('.address_city_list').show()
    }, function () {
        setTimeout(function () {
            //$(this).next().find('.allColor').hide();
            if ($('.address_city_list').is(':hover')) {
            } else {
                $('.address_city_list').hide()
            }
        }, 600);
    });
    
    
});


