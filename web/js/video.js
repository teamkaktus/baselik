$(document).ready(function(){
    var paginations = {
        limit: 4,
        offset: 0
    };
    var key = 'AIzaSyBJHF3o8WiIfGVUb1wmYJSpGVVHrHFq1PU';
    var vid = $(".without_video_id").val();
    var urlReq = 'https://www.googleapis.com/youtube/v3/videos?id='+vid+'&key='+key+'&part=snippet,contentDetails,statistics';
    console.log("Request URI:",urlReq);
    $.ajax({
        type: "GET",
        url: urlReq,
        data: { b:true }
    }).done(function( response ){
        var dateAdd = '',
            countViewed = '';

        var dateObj = new Date(response.items[0].snippet.publishedAt),
            year = dateObj.getFullYear(),
            month = dateObj.getMonth() + 1,
            date = dateObj.getDate();

        if (month < 10) {
            month = '0' + month;
        }
        if (date < 10) {
            date = '0' + date;
        }

        dateAdd =  date + '.' + month + '.' + year;
        countViewed = response.items[0].statistics.viewCount.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, " ");
        $("#date-created").html(dateAdd);
        $("#date-wived").html(countViewed);

    }).error(function(e){
        console.log("Response Errors:",e);
        return -2;
    }).fail(function(e){
        console.log("Response Fail:",e);
    }).always(function(){

    });
    
    function geteAnotherVideos(){
        var category_id = $('.GetCategory_id').val();
        var without_video_id = $('.without_video_id').val();
        $.ajax({
            type: 'POST',
            url: '../../../../geteanothervideos',
            data: {
                category_id:category_id,
                without_video_id:without_video_id,
                limit: paginations.limit,
                offset: paginations.offset
            },
            dataType: "json",
            success: function(response){
                if(response.length < 4)
                    $(".show_more_video_btn").hide();
                //$('.videoBoxContent').html('');
                for(var key in response){
                    $('.videoBoxContent').append("<div class='video_item prem'>"+
                        "<a class='video_link' href='../../../video/"+response[key].id+"'>"+
                            "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                            '<p class="vedeo_art">'+response[key].categoryName+'</p>'+
                            '<p class="vedeo_text">'+response[key].username+'</p>'+
                        '</a>'+
                    '</div>');
                }
            }
        });
    }
    geteAnotherVideos();

    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        geteAnotherVideos();
        e.preventDefault();
    });
    
    
    if($.cookie('videos') == undefined){
        $.cookie('videos','');
    }
    
    var video_id = $('.favotite').data('video_id');
    var stringCookieVideo = $.cookie('videos');
    var arrayVideo = stringCookieVideo.split(",");
    if($.inArray(video_id.toString(), arrayVideo) !== -1){
        $('.favotite').text('Удалить из избранного');
        $('.favotite').removeClass('vs_fav');
        $('.favotite').addClass('vs_fav_active');
    }
    
    
    
    $(document).on('click','.favotite',function(){
        var video_id = $(this).data('video_id');
        
        var stringCookieVideo = $.cookie('videos');
        var arrayVideo = stringCookieVideo.split(",");
        var date = new Date();
        date.setTime(date.getTime() + (366* 24 * 60 * 60 * 1000));
//        
        if($.inArray(video_id.toString(), arrayVideo) !== -1){
            arrayVideo.splice( $.inArray(video_id, arrayVideo), 1 );
            $('.countFavorite').text(arrayVideo.length - 1);
            var stringVideo  = arrayVideo.join(',');
            $.cookie("videos", stringVideo, { path: '', expires: date});
            $.cookie("videos", stringVideo, { path: '/', expires: date});
            $.cookie("videos", stringVideo, { path: '/video', expires: date});
            $.cookie("videos", stringVideo, { path: '/video/', expires: date});
            $.cookie("videos", stringVideo, { path: '/favorites', expires: date});
            $.cookie("videos", stringVideo, { path: '/category', expires: date});
            $.cookie("videos", stringVideo, { path: '/category/', expires: date});
            $.cookie("videos", stringVideo, { path: '/user/', expires: date});
            $.cookie("videos", stringVideo, { path: '/videosearch', expires: date});
            $.cookie("videos", stringVideo, { path: '/allvideo/', expires: date});
            $(this).text('Добавить в избранное');
            $('.favotite').removeClass('vs_fav_active');
            $('.favotite').addClass('vs_fav');
        }else{
            arrayVideo.push(video_id);
            var stringVideo  = arrayVideo.join(',');
            $('.countFavorite').text(arrayVideo.length - 1);
            $.cookie("videos", stringVideo, { path: '', expires: date});
            $.cookie("videos", stringVideo, { path: '/', expires: date});
            $.cookie("videos", stringVideo, { path: '/video', expires: date});
            $.cookie("videos", stringVideo, { path: '/video/', expires: date});
            $.cookie("videos", stringVideo, { path: '/favorites', expires: date});
            $.cookie("videos", stringVideo, { path: '/category', expires: date});
            $.cookie("videos", stringVideo, { path: '/category/', expires: date});
            $.cookie("videos", stringVideo, { path: '/user/', expires: date});
            $.cookie("videos", stringVideo, { path: '/videosearch', expires: date});
            $.cookie("videos", stringVideo, { path: '/allvideo/', expires: date});
            $(this).text('Удалить из избранного');
            $('.favotite').removeClass('vs_fav');
            $('.favotite').addClass('vs_fav_active');
        }
//            
            console.log($.cookie());
        
    });
    
    
});
