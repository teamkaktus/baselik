$(document).ready(function(){
    var paginations = {
        limit: 4,
        offset: 0
    };
    
    if($.cookie('videos') == undefined){
        $.cookie('videos','');
    }else{
        getFavoritesVideo();
    }

    function getFavoritesVideo(){
        var stringCookieVideo = $.cookie('videos');
        var arrayVideo = stringCookieVideo.split(",");
        console.log(arrayVideo);
        $.ajax({
            type: 'POST',
            url: '../../../../getfavoritevideo',
            data: {
                arrayVideo:arrayVideo,
                limit: paginations.limit,
                offset: paginations.offset
            },
            dataType: "json",
            success: function(response){
                //console.log(response);
                if(response.length < 4)
                    $(".show_more_video_btn").hide();
                for(var key in response){
                    console.log(response[key]);
                    $('.videoBoxContent').append("<div class='video_item prem video_item"+response[key].id+"'>"+
                        "<a class='video_link' href='../../../video/"+response[key].id+"'>"+
                            "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                                '<button class="del_vid del_vid_favorite clf" data-video_id="'+response[key].id+'" >'+
                                    '<img src="img/ico/tresh.png" alt="img">'+
                                '</button>'+
                            '<p class="vedeo_art">'+response[key].categoryName+'</p>'+
                            '<p class="vedeo_text">'+response[key].username+'</p>'+
                        '</a>'+
                        '</div>');
                }
            }
        });
    }

    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        getFavoritesVideo();
        e.preventDefault();
    });
    
    $(document).on('click','.del_vid_favorite',function(e){
        e.preventDefault();
        console.log($.cookie('videos'));
        var video_id = $(this).data('video_id');
        var stringCookieVideo = $.cookie('videos');
        var arrayVideo = stringCookieVideo.split(",");
        var newVideoArray = [];
        for(var key in arrayVideo){
            if(arrayVideo[key] != video_id.toString()){
                newVideoArray.push(arrayVideo[key]);
            }
        }
        //console.log(arrayVideo);
//        console.log(arrayVideo.indexOf(video_id.toString(),1));
//        arrayVideo.splice(2);
        var date = new Date();
        date.setTime(date.getTime() + (366* 24 * 60 * 60 * 1000));
        var stringVideo  = newVideoArray.join(',');
        $('.countFavorite').text(newVideoArray.length - 1);
        $.cookie("videos", stringVideo, { path: '', expires: date});;
        $.cookie("videos", stringVideo, { path: '/', expires: date});;
        $.cookie("videos", stringVideo, { path: '/favorites', expires: date});
        $.cookie("videos", stringVideo, { path: '/category', expires: date});
        $.cookie("videos", stringVideo, { path: '/category/', expires: date});
        $.cookie("videos", stringVideo, { path: '/user/', expires: date});
        $.cookie("videos", stringVideo, { path: '/video/', expires: date});
        $.cookie("videos", stringVideo, { path: '/video', expires: date});
        $.cookie("videos", stringVideo, { path: '/videosearch', expires: date});
        $.cookie("videos", stringVideo, { path: '/allvideo/', expires: date});
        $('.video_item'+video_id).remove();
        console.log($.cookie('videos'));
    });
    
    
});
