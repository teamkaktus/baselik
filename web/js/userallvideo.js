$(document).ready(function() {
    var paginations = {
        limit: 4,
        offset: 0
    };
    function getDataVideos(username){
        $.ajax({
            type: 'POST',
            url: '../../../../getuserallvideo',
            data: {
                username:username,
                limit: paginations.limit,
                offset: paginations.offset,
            },
            dataType: "json",
            success: function(response){
                if(response.length < 4){
                    $(".show_more_video_btn").hide();
                }else{
                    $(".show_more_video_btn").show();
                }
                console.log(response);
                for(var key in response){
                    $('.videoBoxContent').append("<div class='video_item prem'>"+
                        "<a class='video_link' href='../../../video/"+response[key].id+"'>"+
                            "<img class='video_img' src='//img.youtube.com/vi/"+response[key].video_src+"/0.jpg'  alt='img'>"+
                            '<p class="vedeo_art">'+response[key].categoryName+'</p>'+
                            '<p class="vedeo_text">'+response[key].username+'</p>'+
                        '</a>'+
                    '</div>');
                }
            }
        });
    }
    getDataVideos($('.UserUsername').val());

    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        getDataVideos($('.UserUsername').val());
        e.preventDefault();
    });
    
//    $(document).on('change','.selectCity',function(){
//        if($(this).val() != ''){
//            paginations = {
//                limit: 4,
//                offset:0
//            };
//            $('.videoBoxSearchContent').html('');
//            $(".show_more_video_btn").show();
//            getDataVideos($('.videosearchtext').val(),$(this).val());
//        }
//    });
    
});


