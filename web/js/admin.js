/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
   
    $(document).on('click',".categoryUpdate",function(){
        $('#updateCategoryBlock').modal('show');
        $('#updateCategoryBlock').find('#inputUpdateCategory').val($(this).parent().parent().find('#category_name').text());
        $('#updateCategoryBlock').find('#inputUpdateCategoryId').val($(this).data('id'));
    });
    
    $(document).on('click',".categoryDelete",function(){
        var category_id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '../../../../admin/ajaxcategorydelete',
            data: {category_id:category_id},
            dataType: "json",
            success: function(response){
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        });
    });
    
    
    $(document).on('click',"#saveUpdateCategory",function(){
        var category_name = $(this).parent().find('#inputUpdateCategory').val();
        var category_id = $(this).parent().find('#inputUpdateCategoryId').val();
        
        $.ajax({
            type: 'POST',
            url: '../../../../admin/ajaxcategoryupdate',
            data: {category_name:category_name,category_id:category_id},
            dataType: "json",
            success: function(response){
                if(response.status == 'success'){
                    $('#updateCategoryBlock').modal('hide');
                    $('#categoryUpdate'+category_id).parent().find('#category_name').text(category_name)
                    swal("Категория редактирована!!", "", "success");
                }
            }
        });
    });
    
});