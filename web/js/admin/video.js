$(function () {
    $(".RemoveFile").click(function (e) {
        var id = $(this).attr('data-id');
        var status_desc = '';
        swal({
                title: "Удаление видео!",
                text: "Укажите, пожалуйста причину удаления видео:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Здесь опишите причину"
            },
            function (inputValue) {
                if (inputValue === false) return false;

                if (inputValue === "") {
                    swal.showInputError("Причина обязательна");
                    return false
                }

                status_desc = inputValue;
                $.ajax({
                    type: 'POST',
                    url: '../../../../admin/removevideo/' + id,
                    data: {
                        status_desc: status_desc
                    },
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        $("#video_" + response).remove();
                        swal("Видео Удалено!", 'Пользователь получит уведомление об удалении видео с описанием причины', "success");
                    }
                });
                return true
            });
        e.preventDefault();
    });
});
