$(document).ready(function(){
        function deleteFile(file_name){
            $.ajax({
                type: 'POST',
                url: '../../../deleteavatar',
                data: {file_name:file_name},
                success: function(response){
                }
            });
        }

        var myDropzone = new Dropzone($('.avatar')[0], {
            url:'../../../uploadavatar',
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                //$('.user_header_photo').find('.dz-preview').hide();
                $('.avatar').attr('src','/img/users_images/'+response.xhr.response);
                //$('#userAvatarWrapper').val(response.xhr.response);
                $.ajax({
                    type: 'POST',
                    url: '../../../addavatar',
                    data: {avatar_src:response.xhr.response},
                    dataType:'json',
                    success: function(response){
                        if(response == 'save'){
                            swal("Сохранено!", "", "success")
                        }else{
                            swal("При сохранении возникла ошибка!", "", "warning")
                        }
                    }
                });
                
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               deleteFile(response.xhr.response);
            }
        });
});