$(document).ready(function() {
    
    $(document).on('click','.video_link',function(e){
        if($(this).data('video_status') == 0){
            e.preventDefault();
        }
    });
    
    
    function getDataVideos(){
        $.ajax({
            type: 'POST',
            url: '../../../../getprofilevideo',
            data: {},
            dataType: "json",
            success: function(response){
                $('.videoBoxContent').html('');
                for(var key in response){
                    console.log(response[key].dateCreate);
                    if(response[key].status != 2){
                        var link = '';
                        var tooltip = '';
                        var video_status = '0';
                        var video_src = '../../../img/video/0.jpg';
                        var video_del = '<button class="del_vid clf" data-video_id="'+response[key].id+'">'+
                            '<img src="../img/ico/tresh.png" alt="img">'+
                        '</button>';
                        var video_stat = '';
                        if(response[key].status == 1){
                            link = '../../../video/'+response[key].id;
                            video_status = '1';
                            video_src = '//img.youtube.com/vi/'+response[key].video_src+'/0.jpg';
                            video_stat = '<span class="published_vid clf">Опубликовано</span>';
                        }
                        
                        if(response[key].status == 3){
                            var tooltip = "data-toggle='tooltip' title='<h3 style=margin-top:0px;><b>Видео удалено администратором</b></h3>"+response[key].status_description+"'";
                            var video_src = '../../../img/video_delete.png';
                            video_stat = '<span class="published_vid clf">Удалено</span>';
                        }
                        if(response[key].status == 0){
                            video_stat = '<span class="published_vid clf">На модерации</span>';
                        }
                        
                        $('.videoBoxContent').append("<div class='video_item prem video_item"+response[key].id+"'>"+
                            "<a "+tooltip+" class='tooltipDesign video_link' data-video_status='"+video_status+"' href='"+link+"'>"+
                                "<img class='video_img' src='"+video_src+"'  alt='img'>"+
                                video_del+
                                video_stat+
                            '</a>'+
                        '</div>');
                    }
                }
            }
        });
    }
    getDataVideos();
    
    $(document).on('click','.del_vid',function(e){
        e.preventDefault();
        var video_id = $(this).data('video_id');
        $.ajax({
            type: 'POST',
            url: '../../../../removevideouser',
            data: {video_id:video_id},
            dataType: "json",
        }).done(function(response){
            if(response.status == 'success'){
                $('.video_item'+video_id).remove();
            }
        });
    });
});