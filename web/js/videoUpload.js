$(document).ready(function(){

    var arrayVideo = [];
    
        var previewNode = document.querySelector("#previews_SS");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);
        
        var myDropzone = new Dropzone($('.videoUploadClick')[0], { 
            url: "../../savedropedfile",
            maxFilesize:10000000,
            acceptedFiles:'video/*',
            clickable: ".ulpoad_btn, .video_upload, .upload_text",
            previewTemplate:previewTemplate,
            previewsContainer: '.previewsVideoContainer',
        });
        
        function deleteFile(file_name){
            $.ajax({
                type: 'POST',
                url: '../../../deletefile',
                data: {file_name:file_name},
                success: function(response){
                }
            });
        }
        
        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                var d = new Date();
                var n = d.getTime();
                
                arrayVideo.push({i:n,name:response.xhr.response});
                response.previewElement.id = n;
                $('.videoUploadSave').show();
                //console.log(arrayVideo);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
                var removeItem = response.xhr.response;
                arrayVideo = jQuery.grep(arrayVideo, function(value) {
                    return value.name != removeItem;
                });
                
                //console.log(arrayVideo);
                deleteFile(response.xhr.response);
                if(myDropzone.files.length == 0){
                    $('.videoUploadSave').hide();
                }
            }
        });
        
        $(document).on('click','.videoUploadSave',function(){
            var arrayVideoForSave = [];
            for(var key in arrayVideo){
                var objectVideo = arrayVideo[key];
                arrayVideoForSave[arrayVideoForSave.length] = {
                    videoCategory:$('#'+objectVideo.i).find('[name="videoCategory"]').val(),
                    videoDescription:$('#'+objectVideo.i).find('#videoDescription').val(),
                    videoName:objectVideo.name
                }
            }
            $.ajax({
                type: 'POST',
                url: '../../../ajaxvideoadd',
                data: {arrayVideoForSave:arrayVideoForSave},
                dataType:'json',
                success: function(response){
                    if(response.status == "success"){
                        document.location.reload(true);
                    }else{
                        alert('error');
                    }
                }
            });
        });

});
