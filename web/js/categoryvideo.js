$(document).ready(function () {

    var paginations = {
        limit: 2,
        offset: 0,
        category_id: ''
    };

    function getDataVideos(category_id,cityFilter) {
//        alert('category - '+category_id);
//        alert('city - '+cityFilter);
//        alert('limit - '+paginations.limit);
//        alert('offset - '+paginations.offset);
        if(cityFilter == undefined){
            cityFilter = 'empty';
        }
        $.ajax({
            type: 'POST',
            url: '../../../../getdatavideo',
            data: {
                cityFilter: cityFilter,
                category_id: category_id,
                limit: paginations.limit,
                offset: paginations.offset
            },
            dataType: "json",
            success: function (response) {
                if(response.length < 2){
                    $(".show_more_video_btn").hide();
                }else{
                    $(".show_more_video_btn").show();
                }
                for (var key in response) {
                    console.log(response[key]);
                    $('.videoBoxContent').append("<div class='video_item prem'>" +
                        "<a class='video_link' href='../../../video/" + response[key].id + "'>" +
                        "<img class='video_img' src='//img.youtube.com/vi/" + response[key].video_src + "/0.jpg'  alt='img'>" +
                        '<p class="vedeo_art">' + response[key].categoryName + '</p>' +
                        '<p class="vedeo_text">' + response[key].username + '</p>' +
                        '</a>' +
                        '</div>');
                }
            }
        });
    }
    
    var hash = window.location.hash.replace("#", "");
    if (hash == '') {
        paginations.category_id = $('.profession_box').find('.cat_actv').data('category_id');
        //console.log(paginations.category_id);
        getDataVideos($('.profession_box').find('.cat_actv').data('category_id'),$.session.get("cityTop"));
    } else {
        paginations.category_id = hash;
        //console.log(paginations.category_id);
        getDataVideos(hash,$.session.get("cityTop"));
        $('.categoryLi' + hash).addClass('choisen');
    }
    $(".show_more_video_btn").click(function(e){
        paginations.offset += paginations.limit;
        getDataVideos(paginations.category_id,$.session.get("cityTop"));
        e.preventDefault();
    });

    $(document).on('click', '.categoryClick', function () {
        paginations.limit = 2;
        paginations.offset = 0;
        paginations.category_id = $(this).data('category_id');
        getDataVideos($(this).data('category_id'),$.session.get("cityTop"));
        //$(".show_more_video_btn").show();
        var hash = window.location.hash = $(this).data('category_id');
        $('[class ^= categoryLi]').removeClass('choisen');
        $('.categoryLi' + $(this).data('category_id')).addClass('choisen');
        $('.videoBoxContent').html('');
    });

    $(document).on('change','.selectCity',function(){
        if($(this).val() != ''){
            paginations.limit = 2;
            paginations.offset = 0;
            
            $('.videoBoxContent').html('');
            //$(".show_more_video_btn").show();
            //console.log(paginations.category_id);
            getDataVideos(paginations.category_id,$(this).val());
        }
    });
});


