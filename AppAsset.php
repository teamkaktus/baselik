<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "css/bootstrap-theme.css",
        "css/style.css",
        "css/normalize.css",
        "css/jquery.arcticmodal.css",
        "css/themes/dark.css",
        "css/jquery.formstyler.css",
        "css/owl/owl.carousel.css",
        "css/media.css",
        "fonts/font.css"
    ];
    public $js = [
        "js/html5shiv.min.js",
        "js/respond.min.js",
        "js/placeholders.js",
        "js/jquery.formstyler.js",
        "js/jquery.arcticmodal.js",
        "js/owl.carousel.min.js",
        "js/script.js",
        "js/main.js"
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}