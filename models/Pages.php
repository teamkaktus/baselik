<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ideas".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $ideas_description
 * @property integer $stormer_id
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//            $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'addPage' => ['url_page','title','content'],
            'updatePage' => ['content'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url_page','title','content'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
        ];
    }
}
