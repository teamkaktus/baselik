<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ideas".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $ideas_description
 * @property integer $stormer_id
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function scenarios()
    {
        return [
            'addCategory' => ['name','parent_id','url'],
            'updateCategory' => ['name'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name','required'],
            ['url','required'],
            ['url', 'unique'],
            ['url', 'match','pattern'=>'/^[a-zA-Z0-9_]+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
        ];
    }
}
