<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ideas".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $ideas_description
 * @property integer $stormer_id
 */
class Usercategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_category';
    }

//    public function scenarios()
//    {
//        return [
//            'addCategory' => ['name','parent_id','url'],
//            'updateCategory' => ['name'],
//        ];
//    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
        ];
    }
}
