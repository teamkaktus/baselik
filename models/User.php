<?php

namespace app\models;

use \yii\web\IdentityInterface;
use app\models\Usercategory;
use Yii;

class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    public static function tableName()
    {
        return 'users';
    }

    public $profile;
    public $authKey;


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }
        else {
            return self::findOne($id);
        }
    }
    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service) {
    	if (!$service->getIsAuthenticated()) {
    		throw new ErrorException('EAuth user should be authenticated before creating identity.');
    	}
    	$id = $service->getServiceName().'-'.$service->getId();
    	$attributes = array(
    			'id' => $id,
    			'username' => $service->getAttribute('name'),
    			'authKey' => md5($id),
    			'profile' => $service->getAttributes(),
    	);
    	$attributes['profile']['service'] = $service->getServiceName();
    	Yii::$app->getSession()->set('user-'.$id, $attributes);
    	return new self($attributes);
    }

    public static function findByUsername($username)
    {
            return static::findOne(array('username' => $username));
    }
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d");
            $this->user_status = 1;
            
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'login' => ['username', 'password'],
            'registration' => ['username', 'email', 'password','password_hash'],
            'social_login' => ['username', 'email', 'password','password_hash', 'first_name', 'gender', 'users_type', 'last_name'],
            'update' => ['username', 'email', 'gender','birthday'],
            'change_avatar' => ['avatar'],
            'change_password' => ['password_hash'],
            'ban' => ['user_status','reason_ban'],
            'default' => [],
	    ];
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'unique'],
        ];
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    static function categoryIsSelected($category_id,$user_id){
        $modelUserCategory = Usercategory::find()->where(['category_id' => $category_id, 'user_id' => $user_id])->one();
        if($modelUserCategory == null){
            return 'no';
        }else{
            return 'yes';
        }
    }
    
    static function getName($user_id){
        $modelUser = User::find()->where(['id' => $user_id])->one();
        return $modelUser->username;
    }
    
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
            //print_r([$expire,$timestamp]);exit;
        return $timestamp + $expire >= time();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

}
