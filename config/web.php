<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
      'eauth' => [
        'class' => 'nodge\eauth\EAuth',
        'popup' => false, // Use the popup window instead of redirecting.
        'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
        'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
        'httpClient' => [
            // uncomment this to use streams in safe_mode
            //'useStreamsFallback' => true,
        ],
        'services' => [
            'facebook' => [
                // register your app here: https://developers.facebook.com/apps/
                'class' => 'nodge\eauth\services\extended\FacebookOAuth2Service',
                'clientId' => '866691656774759',
                'clientSecret' => '8e96514acf1fc4265fbd75afad8152ed',
            ],
            'vkontakte' => [
                // register your app here: https://vk.com/editapp?act=create&site=1
                'class' => 'nodge\eauth\services\extended\VKontakteOAuth2Service',
                'clientId' => '5468879',
                'clientSecret' => 'd7xld4jrh1at7QU0p6l4',
            ],
            'twitter' => array(
              // register your app here: https://dev.twitter.com/apps/new
              'class' => 'nodge\eauth\services\extended\TwitterOAuth1Service',
              'key' => 'QRQUdTF3wMBt2KWYFE2gyUrBJ',
              'secret' => 'l5z5K3bZJptVgB3uyZGSo2uIFRtcb2ea6kaA3mvSYmMWXwCMh3',
            ),
            'mailru' => array(
              // register your app here: http://api.mail.ru/sites/my/add
              'class' => 'nodge\eauth\services\extended\MailruOAuth2Service',
              'clientId' => '744312',
              'clientSecret' => '0c72e52f93740b2f0e17e7702fe31226',
            ),
            'odnoklassniki' => array(
              // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
              // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
              'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
              'clientId' => '1247035392',
              'clientSecret' => '82DBBE9DCA6670D5803FBB82',
              'clientPublic' => 'CBABEFFLEBABABABA',
              'title' => 'Odnoklas.',
            ),
            'linkedin_oauth2' => array(
              // register your app here: https://www.linkedin.com/secure/developer
              'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
              'clientId' => '771xmnvb7f5c19',
              'clientSecret' => 'SE2KvsByKWy13urJ',
              'title' => 'LinkedIn',
            ),
       ],
     ],
     'i18n' => array(
        'translations' => array(
            'eauth' => array(
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@eauth/messages',
            ),
        ),
    ),
      'request' => [
        	'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KIPpz30dWlEb9Km_jokPce0-IU4i6-YE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
          'mailer' => [
              'class' => 'yii\swiftmailer\Mailer',
              // send all mails to a file by default. You have to set
              // 'useFileTransport' to false and configure a transport
              // for the mailer to send real emails.
              'useFileTransport' => false,
              'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'padlasmail@gmail.com',
                'password' => 'S93179317',
                'port' => '465',
                'encryption' => 'ssl',
              ],
          ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'about' => 'site/about',
                'login'=>'site/login',
                'signup'=>'site/signup',
        	'site/requestpasswordreset' => 'site/requestpasswordreset',
        	'site/resetpassword' => 'site/resetpassword',
        	'site/imagetext' => 'site/imagetext',
        	'recomendationvideoemail' => 'site/recomendationvideoemail',
        	'site/ajaxlogin' => 'site/ajaxlogin',
        	'site/ajaxsignup' => 'site/ajaxsignup',
        	'category/<category_url:\w+>' => 'category/index',
        	'video/<video_id:\d+>' => 'category/video',
        	'user/video' => 'user/video',
        	'gethomevideo' => 'category/gethomevideo',
        	'geteanothervideos' => 'category/geteanothervideos',
        	'favorites' => 'category/favorites',
        	'uploadavatar' => 'category/uploadavatar',
        	'deleteavatar' => 'category/deleteavatar',
        	'addavatar' => 'category/addavatar',
        	'getdatavideo' => 'category/getdatavideo',
        	'getfavoritevideo' => 'category/getfavoritevideo',
        	'recomendation' => 'site/recomendation',
        	'videosearch' => 'site/videosearch',
        	'getcategoryname' => 'category/getcategoryname',
        	'getsearchvideo' => 'category/getsearchvideo',
        	'sendemail' => 'site/sendemail',
        	'getprofilevideo' => 'user/getprofilevideo',
        	'ajaxvideoadd' => 'user/ajaxvideoadd',
        	'getuserallvideo' => 'user/getuserallvideo',
        	'user/specialization' => 'user/specialization',
        	'user/contact' => 'user/contact',
        	'allvideo/<username:\w+>' => 'user/allvideo',
        	'user/profile' => 'user/profile',
                'admin/pages' => 'admin/pages',
                'site/vakancui' => 'site/vakancui',
        	'removevideouser' => 'user/removevideouser',
        	'allusercity' => 'site/allusercity',
        	'deletefile' => 'category/deletefile',
        	'savedropedfile' => 'category/savedropedfile',
        	'user/getcity' => 'user/getcity',
        	'user/ajaxcategoryuser' => 'user/ajaxcategoryuser',
        	'user/ajaxsavedataprofile' => 'user/ajaxsavedataprofile',
        	'admin/users' => 'admin/users',
        	'admin/getvideoserver' => 'admin/getvideoserver',
        	'admin/video' => 'admin/video',
        	'admin/ajaxcategorydelete' => 'admin/ajaxcategorydelete',
        	'admin/categorydelete/<category_id:\w+>' => 'admin/categorydelete',
        	'admin/currentvideo' => 'admin/currentvideo',
        	'admin/uploadyoutube' => 'admin/uploadyoutube',
        	'admin/removevideo/<video_id:\d+>' => 'admin/removevideo',
        	'admin/delete-video' => 'admin/deletevideo',
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
