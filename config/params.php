<?php

return [
    'adminEmail' => 'admin@baselik.com',
    'supportEmail' => 'support@baselik.com',
    'user.passwordResetTokenExpire' => '259200',
];
