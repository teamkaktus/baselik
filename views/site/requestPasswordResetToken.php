<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Запрос на восстановление пароля';
?>
<div class="container">
    <div class="breadcrumbMy">
        <ul class="breadcrumb"><li><a href="<?= Url::home(); ?>">Home</a></li>
            <li class="active"><?= $this->title; ?></li>
        </ul>
    </div>
    <?php 
        if(Yii::$app->session->hasFlash('passwordResetEmailNotSend')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Ошибка email',
            ]);
        endif;  
    ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, введите вашу электронную почту. Ссылка для сброса пароля будет отправлена туда</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?= $form->field($model, 'email')->label('Ваш электронный адрес'); ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn','style' => 'border-radius: 3px; color: white; background-color: rgb(119, 191, 82);']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>