<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\assets\HomevideoAsset;

HomevideoAsset::register($this);

$this->title = 'Baselik';
?>

        <?php 
            if(Yii::$app->session->hasFlash('passwordResetEmailSend')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => 'Ссылка для изменения пароля отправлен вам на почту',
                ]);
            endif; 
        ?>

        <div class="serch_wr clf" style="position:relative;">
            <div class="serch_box bs clf">
                <?php $form = ActiveForm::begin(['action' => Url::home().'videosearch','method' => 'get','class' => 'searchForm']); ?>
                    <input AUTOCOMPLETE="off"  type="text" name="input_search" value="" placeholder="Поиск по специальности или услуге...">
                    <button class="clf fr"></button>
                <?php ActiveForm::end(); ?>
                <div class="box_category_list">
                    <ul class="ul_category">
                    </ul>
                </div>
            </div>
            <p class="fl cb clf">Например: <span class="pruklad"><?= $arrayCategory[array_rand($arrayCategory,1)]; ?></span></p>
        </div>

        <div class="profession_box clf">
            <ul class="clf">
                <?php 
                    $k = 0;
                    foreach($modelCategory as $category){
                    $k = $k + 1; ?>
                <li <?php if($k > 2){ echo 'style="display:none;"';} ?>>
                        <?= HTML::a($category->name,'/category/'.$category->url); ?>
                    </li>
                <?php } ?>
            </ul>

            <a class="profession_more fl" style="cursor:pointer;">Ещё профессии</a> 
        </div>

        <div class="reklam_wr clf">
            <div class="reklam_box_r bs fl clf" style="background-image: url('/site/imagetext?<?= time();?>');">
                <p class="reklab_art">Ищешь клиентов?</p>
                <p class="reklam_text">Загрузи свою видеопрезентацию,<br/> покажи себя!</p>
                    <?= HTML::a('Посмотреть рекомендации<br/> по записи видео',Url::home().'recomendation'); ?>
                <?php if(\Yii::$app->user->isGuest){ ?>
                    <a class="reklam_btn registr hb fl" href="#mod">Загрузить видео</a>
                <?php }else{ ?>
                    <?= HTML::a('Загрузить видео','/user/video',['class'=> 'reklam_btn hb fl']); ?>
                <?php } ?>
                <!--<div class="userCount">
                        <p style="color:white;font: 24px 'BebasNeueRegular';"><b>C НАМИ УЖЕ</b></p>
                        <p style="color:white;font: 30px 'BebasNeueRegular';"><b><?= $userCount; ?></b></p>
                        <p style="color:white;font-size:15px;margin-top:-14px;">человек</p>
                    </div>-->
            </div>
            
            
            
            
            <div style="display: none;">
                <div class="box-modal clf" id="cpecForm">
                    <?php $form = ActiveForm::begin(['id' => 'podbor_spec']); ?>
                        <div class="up_win_art">
                            <p>Заявка на подбор специалиста</p>
                        </div>
                        <div class="up_win_input_box">
                            <input type="text" name="name" value="" placeholder="Имя">
                            <input type="email" name="e_mail" value="" placeholder="E-mail">
                            <input type="text" name="telefon" value="" placeholder="Телефон">
                            <?= Html::dropDownList('s_id', null,$arrayCategory,['class'=> 'category_select']) ?>
                        </div>
                        <?= Html::submitButton('Отправить', ['class' => 'up_win_btn podborSpecForm', 'name' => 'login-button']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            
            
            <div class="reklam_box_l bs fr clf">
                <p class="reklab_art">Ищешь исполнителей?</p>
                <p class="reklam_text">Время не ждет!<br> Просматривай видео  и связывайтесь<br/> со специалистом напрямую!</p>
                <a class="cpecForm" href="#">Или оставьте заявку, и мы<br/> подберём специалиста</a>
            </div>
        </div>

        <div class="video_box videoBoxContent clf" style="margin-top:50px;">
            
        </div>

        <div class="col-sm-12">
            <a class="show_more_video_btn hb cb" href="#">Ещё</a>
        </div>


    
