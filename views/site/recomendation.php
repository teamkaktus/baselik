<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    .wrap{
        padding-bottom: 0px !important;
    }
</style>
<div class="row recpmendationDivStyle" >
    <div class="container">
        <div class="col-sm-12">
            <div class="col-lg-6 col-md-6 pull-right recomendationTextBlockOne" style="font-size:16px;">
                Записать привлекательное видео не так сложно, как может показаться на первый взгляд. Для начала будет достаточно даже встроенной камеры телефона - главное, не вздумайте снимать вертикально! Подготовьте заранее текст, обойдитесь без сложных оборотов и технических подробностей своего дела. Пусть это будет речь, которую вам не придётся заучивать - говорите спокойно и без напряжения. 
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-lg-5 col-md-4 pull-right recomendationTextBlockTwo" style="font-size:16px;">
                Хорошо бы снять видео в рабочем антураже, но если такой возможности нет, то лучше встаньте на фоне пустой стены, нежели в жилой комнате с неубранной постелью. Выберите позицию, в которой освещение будет падать на вас, а не на камеру. Если результаты вашей работы можно продемонстрировать на камеру, обязательно сделайте это.
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-lg-5 col-md-4 recomendationTextBlockThree">
                Не забывайте, что вы всегда можете рассчитывать на нашу помощь в качественной съёмке продающего видеоролика. Студия с профессиональным оборудованием и наши специалисты по рекламе обеспечат вам до <b>50% больше шансов на успех!</b>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6 recomendationTextBlockFoure">
                <h2 style="font: 30px 'BebasNeueRegular';border-bottom:0px;margin-bottom:0px;padding-bottom:0px;text-align:center;"><b>Оставьте свои данные</b></h2>
                <h4 style="text-align:center;">и мы вам обязательно перезвоним</h4>
                
                <input type="text" class="form-control recomendationSendName recomendationBacgroundInput" placeholder="Ваше имя">
                <input type="text" class="form-control recomendationSendTelefon recomendationBacgroundInput" placeholder="Телефон">
                <input type="submit" class="up_win_btn recomendationButtonSend" style="width:100%;background-size: 100%">
                
            </div>
        </div>
    </div>
</div>