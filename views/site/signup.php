<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1>Signup</h1>
    
    <?php $form = ActiveForm::begin(['id' => 'form-signup',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],

            ]); ?>
        <?= $form->field($newModel, 'username') ?>
        <?= $form->field($newModel, 'email') ?>
        <?= $form->field($newModel, 'password')->passwordInput() ?>
        
        <div style="margin-left:150px;" class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    
</div>
