<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Сброс пароля';
?>
<div class="container">
    <div class="breadcrumbMy">
        <ul class="breadcrumb"><li><a href="<?= Url::home(); ?>">Home</a></li>
            <li class="active"><?= $this->title; ?></li>
        </ul>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Пожалуйста, введите новый пароль:</p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput()->label('Введите новый пароль'); ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn','style' => 'border-radius: 3px; color: white; background-color: rgb(119, 191, 82);']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
