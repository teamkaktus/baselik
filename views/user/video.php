<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\assets\VideouploadAsset;
use app\assets\VideoprofileAsset;

VideoprofileAsset::register($this);
VideouploadAsset::register($this);

$this->title = 'Мои видео';
?>
<div style="display:none;">
    <div class="table table-striped" class="files" id="previews_SS" style="margin-bottom: 0px;padding-top:10px;">
        <div id="template" class="file-row">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-lg-4 col-sm-4 col-xs-6">
                        <p class="name" data-dz-name></p>
                        <strong class="error text-danger" data-dz-errormessage></strong>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-xs-6">
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            <p class="size" data-dz-size ></p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-xs-7">
                        <?= Html::dropDownList('videoCategory', null, $arrayCategoryChoisen,['class' => 'form-control']) ?>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-xs-5" style="padding-right:0px;">
                        <button data-dz-remove class="btn delete" style="width:100%;border-radius:3px;background-color: #d9534f;color:white;">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Удалить</span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" id="videoDescription" rows="3" placeholder="Краткая информация о видео"></textarea>
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>


    <!--Pers.Cab Content Box-->
    <div class="pers_cab_box bw clf">
        <!-- Nav tabs -->
        <?= $this->render('menu'); ?>
        
        <ul id='nav_tabs_right' class="tab-content fr clf">
            <div role="tabpanel" class="tab-pane active" id="tid4">
                    
                    <div class="video_upload videoUploadClick clf">
                        <a class="ulpoad_btn clf" href="#upl1">Загрузить<br/> видеоролик</a>
                        <p class="upload_text">Чтобы начать загрузку, выберите файл на компьютере или перетащите видеозапись в это окно </p>
                    </div>
                    
                    <div class="previewsVideoContainer" style="width:100%;">
                        
                    </div>
                    <div>
                        <button class="btn videoUploadSave pull-right" style="border-radius:3px;background-color: #77bf52;color:white;display:none;">
                            <span class="fa fa-floppy-o"></span>
                            Сохранить
                        </button>
                    </div>
                    <div style="clear:both;"></div>

                    
                    <div id="vb4" class="video_box videoBoxContent clf" style="margin-top: 50px">
                                           
                    </div>
            </div>
        </ul>
        
    </div>
    