<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="serch_wr clf">
    <div class="serch_box bs clf">
            <input  type="text" name="" value="" placeholder="Поиск по специальности или услуге...">
            <button class="clf fr"></button>
        </div>
        <p class="fl cb clf">Например: <span>Усиление проемов металлическим профилем</span></p>
    </div>

    <!--Pers.Cab Content Box-->
    <div class="pers_cab_box bw clf">
        <!-- Nav tabs -->
        <?= $this->render('menu'); ?>
        
        <ul id='nav_tabs_right' class="tab-content fr clf">
            <div role="tabpanel" class="tab-pane active" id="tid1">
                <!--User ID box TOP-->
                <div class="id_user_box_top fl clf">
                    <img class="clf" src="img/a_page_ava.jpg" alt="img">

                    <div class="user_info_box fl clf">
                        <p class="user_inf_name">Антонова Валерия<br/> Владимировна</p>
                        <p class="user_inf_reg">Дата регистрации: 12.06.2013г.</p>
                    </div>
                </div>

                <!--User ID box BOT-->
                <div class="id_user_box_bot fl clf">
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Пол:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="женский" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Дата рождения:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="17.11.1981г. (33 года)" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">На сайте:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="3 года 6 месяцев" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Количество специализаций:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="2" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Количество открытий контактов:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="216" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Логин в системе:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="valeriya" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">E-mail указанный при регистрации:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="valery@mail.ru" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box PASS ROW-->
                    <div class="id_user_box_text pass_fild clf">
                        <p class="user_box_text clf fl">Новый пароль:</p>
                        <div class="user_box_text_input clf fl">
                            <input class="inp_pass" type="text" name="" value="hgTyUhnHgf" placeholder="" disabled="disabled">
                            <img class="inpt_stat" src="img/ico/checks.png" alt="img">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box PASS ROW-->
                    <div class="id_user_box_text pass_fild clf">
                        <p class="user_box_text clf fl">Подтверждение нового пароля:</p>
                        <div class="user_box_text_input clf fl">
                            <input class="inp_pass" type="text" name="" value="hgTyUhnHgf" placeholder="" disabled="disabled">
                            <!-- <span class="inpt_stat"><img src="img/ico/checks.png" alt="img"></span> -->
                            <img class="inpt_stat" src="img/ico/checks.png" alt="img">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                        <!-- <a class="save_btn fl hb clf" href="#save">Сохранить</a> -->
                    </div>

                    <a class="save_btn dn_n fr hb clf" href="#save">изменить</a>

                </div>
            </div>
        </ul>
    </div>