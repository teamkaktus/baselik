<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\assets\UserallvideoAsset;

UserallvideoAsset::register($this);

$this->title = 'Видео';
?> 
<input type="hidden" class="UserUsername" value="<?= $username; ?>">
    <div class="serch_wr clf" style="position:relative;">
        <div class="serch_box bs clf">
            <?php $form = ActiveForm::begin(['action' => Url::home().'videosearch','method' => 'get']); ?>
                <input AUTOCOMPLETE="off"  type="text" name="input_search" value="" placeholder="Поиск по специальности или услуге...">
                <button class="clf fr"></button>
            <?php ActiveForm::end(); ?>
            <div class="box_category_list" style="z-index:999;border: 1px solid #b9b2ad;background-color:white;position:absolute;width:100%;">
                <ul class="ul_category">
                </ul>
            </div>
        </div>
        <p class="fl cb clf">Например: <span class="pruklad"><?= $arrayCategory[array_rand($arrayCategory,1)]; ?></span></p>
    </div>

    <div class="video_box videoBoxContent clf" style="margin-top: 20px">

    </div>
    <div class="col-sm-12">
        <a class="show_more_video_btn hb cb" style="display:none;" href="#">Ещё</a>
    </div>