<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<?php    
    $class = ($this->context->getRoute() == 'user/profile')?'active':''; 
    $class1 = ($this->context->getRoute() == 'user/specialization')?'active':''; 
    $class2 = ($this->context->getRoute() == 'user/contact')?'active':''; 
    $class3 = ($this->context->getRoute() == 'user/video')?'active':''; 
?>

<ul id='nav_tabs_left' class="nav nav-tabs fl clf" role="tablist">
    <li id="tab_nav1" role="presentation" class="<?= $class; ?>"><a href="/user/profile" aria-controls="home" role="tab" data-toggle="tab">Мои данные</a></li>
    <li id="tab_nav2" role="presentation" class="<?= $class1; ?>"><a href="/user/specialization" aria-controls="profile" role="tab" data-toggle="tab">Специализация</a></li>
    <li id="tab_nav3" role="presentation" class="<?= $class2; ?>"><a href="/user/contact" aria-controls="messages" role="tab" data-toggle="tab">Контакты</a></li>
    <li id="tab_nav4" role="presentation" class="<?= $class3; ?>"><a href="/user/video" aria-controls="settings" role="tab" data-toggle="tab">Видео</a></li>
</ul>