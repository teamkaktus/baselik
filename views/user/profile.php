<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\ProfileAsset;
use yii\helpers\Url;
use app\assets\UploadavatarAsset;

UploadavatarAsset::register($this);
ProfileAsset::register($this);
$this->title = 'Мои данные';
?>

    <!--Pers.Cab Content Box-->
    <div class="pers_cab_box bw clf">
        <!-- Nav tabs -->
        <?= $this->render('menu'); ?>
        
        <ul id='nav_tabs_right' class="tab-content fr clf">
            <div role="tabpanel" class="tab-pane active" id="tid1">
                <!--User ID box TOP-->
                <div class="id_user_box_top fl clf">
                    <?php 
                        if($modelUser->avatar == ''){
                            $avatar = 'img/default_avatar.png';
                        }else{
                            $avatar = 'img/users_images/'.$modelUser->avatar;
                        }
                    ?>
                    <div>
                        <img class="clf avatar" src="<?= Url::home().$avatar; ?>" alt="img">
                    </div>

                    <div class="user_info_box fl clf">
                        <p class="user_inf_name"><span id="lastname"><?= $modelUser->lastname; ?></span> <span id="name"><?= $modelUser->name; ?></span><br/> <span id="surname"><?= $modelUser->surname; ?></span></p>
                        <p class="user_inf_reg">Дата регистрации: <?= $modelUser->date_create; ?></p>
                    </div>
                </div>

                <!--User ID box BOT-->
                <div class="id_user_box_bot fl clf">
                    <!--User ID box ROW-->
                    
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Фамилия:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="lastname" data-model_name="profile" data-field_name="lastname" value="<?= $modelUser->lastname; ?>" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Имя:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="name" data-model_name="profile" data-field_name="name" value="<?= $modelUser->name; ?>" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Отчество:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="surname" data-model_name="profile" data-field_name="surname" value="<?= $modelUser->surname; ?>" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Пол:</p>
                        <div class="user_box_text_input clf fl">
                            <?php 
                                $attr = '';
                                $attr2 = '';
                                if($modelUser->gender == '0'){
                                    $attr = 'selected';
                                }else{
                                    $attr2 = 'selected';
                                }
                            ?>
                            <select name="gender" data-model_name="profile" data-field_name="gender" value="<?= $modelUser->gender; ?>"  disabled="disabled">
                                <option <?= $attr; ?> value="0">женский</option>
                                <option <?= $attr2; ?> value="1">мужской</option>
                            </select>
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Дата рождения:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="date" name="birthday" data-model_name="profile" data-field_name="birthday" value="<?= $modelUser->birthday; ?>" placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">На сайте:</p>
                        <div class="user_box_text_input clf fl">
                            <?php 
                            $time = $modelUser->date_create;
                            $datetime1 = date_create($time);
                            $datetime2 = date_create('now',new DateTimeZone('Europe/Moscow'));
                            $interval = date_diff($datetime1, $datetime2);
                            $date_s = '';
                            if($interval->y == 0){
                                if($interval->m == 0){
                                    $date_s = $interval->d.' дней';
                                }else{
                                    $date_s = $interval->m.' месяцев '.$interval->d.' дней';
                                }
                            }else{
                                $date_s = $interval->y.' лет '.$interval->m.' месяцев '.$interval->d.' дней';
                            }
                            //var_dump($interval);
                            ?>
                            <input type="text" name="" value="<?= $date_s; ?>" placeholder="" disabled="disabled">
                            <!--<span class='dn_n hb edit_btn'>изменить</span> -->
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Количество специализаций:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="<?= $specializationCount; ?>" placeholder="" disabled="disabled">
                            <!--<span class='dn_n hb edit_btn'>изменить</span> -->
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <!--<div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Количество открытий контактов:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="" value="2" placeholder="" disabled="disabled">
                            <!--<span class='dn_n hb edit_btn'>изменить</span> -->
                       <!-- </div>
                    </div>-->
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">Логин в системе:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="username" data-model_name="profile" data-field_name="username" value="<?= $modelUser->username; ?>"  placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <!--User ID box ROW-->
                    <div class="id_user_box_text clf">
                        <p class="user_box_text clf fl">E-mail указанный при регистрации:</p>
                        <div class="user_box_text_input clf fl">
                            <input type="text" name="email" data-model_name="profile" data-field_name="email" value="<?= $modelUser->email; ?>"  placeholder="" disabled="disabled">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>
                    </div>
                    <div class="changePassword">
                    <!--User ID box PASS ROW-->
                        <div class="password">
                            <p style="width: 366px;font: 20px 'proxima_nova_rgbold';color: #323030;text-align: left;float:left;padding: 7px 0 7px 9px;">Новый пароль:</p>
                            <div style="float:left !important;">
                                <input class="inp_pass newPassword" style="font: 20px 'proxima_nova_ltlight';padding: 7px 0 7px 9px;background-color:white;" type="text" name="" value="" placeholder="Введите новий пароль" disabled="disabled">
                                <!--<img class="inpt_stat" src="/img/ico/checks.png" alt="img">-->
                                <span class='editPass' style="cursor:pointer;color:white;">изменить</span>
                            </div>
                        </div>
                        <div class="password confirmPass" style="display:none;">
                            <p style="width: 366px;font: 20px 'proxima_nova_rgbold';color: #323030;text-align: left;float:left;padding: 7px 0 7px 9px;">Подтверждение нового пароля:</p>
                            <div style="float:left !important;">
                                <input class="inp_pass act_inpt  newPasswordConfirm" style="font: 20px 'proxima_nova_ltlight';padding: 7px 0 7px 9px;background-color:white;" type="text" name="" value="" placeholder="Введите новий пароль">
                                <!--<img class="inpt_stat" src="/img/ico/checks.png" alt="img">-->
                                <button class="btn  savePass pull-right" style="border-radius:0px;background-color: #77bf52;color:white;margin-left:10px;">
                                    <span class="fa fa-floppy-o"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                    <!--User ID box PASS ROW-->
                    <!--<div class="id_user_box_text pass_fild clf confirmPass" style="display:none;">-->
<!--                        <p class="user_box_text clf fl"></p>
                        <div class="user_box_text_input clf fl">
                            <input class="inp_pass act_inpt" type="text" name="" value="hgTyUhnHgf" placeholder="" disabled="disabled">
                             <span class="inpt_stat"><img src="img/ico/checks.png" alt="img"></span> 
                            <img class="inpt_stat" src="/img/ico/checks.png" alt="img">
                            <span class='dn_n hb edit_btn'>изменить</span>
                        </div>-->
                        <!-- <a class="save_btn fl hb clf" href="#save">Сохранить</a> -->
                    <!--</div>-->

                    <a class="save_btn dn_n fr hb clf" href="#save">изменить</a>

                </div>
            </div>
        </ul>
    </div>