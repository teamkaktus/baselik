<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\ProfileAsset;

ProfileAsset::register($this);

$this->title = 'Контакты';
?>

    <!--Pers.Cab Content Box-->
    <div class="pers_cab_box bw clf">
        <!-- Nav tabs -->
        <?= $this->render('menu'); ?>
        
        <ul id='nav_tabs_right' class="tab-content fr clf">
            <div role="tabpanel" class="tab-pane active" id="tid3">
                    <!--User ID box BOT-->
                    <div id="id_cont" class="id_user_box_bot fl clf">
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Местонахождение(город):</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="address_city" data-model_name="contact" data-field_name="address_city" value="<?= $modelContact->address_city; ?>" placeholder="" disabled="disabled">
                                <ul class="address_city_list" style="display:none;">

                                </ul>
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Контактный телефон:</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="telefon" data-model_name="contact" data-field_name="telefon" value="<?= $modelContact->telefon; ?>" placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">E-mail</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="email" data-model_name="profile" data-field_name="email" value="<?= $modelUser->email; ?>"  placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Время работы:</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="start_date" id="timepicker" data-model_name="contact" data-type="time"  data-field_name="time_work_start" value="<?= $modelContact->time_work_start; ?>" placeholder="" disabled="disabled">
                                <input type="text" name="end_date" id="timepicker_2" data-model_name="contact" data-type="time" data-field_name="time_work_end" value="<?= $modelContact->time_work_end; ?>" placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Whatsapp:</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="whatsapp" data-model_name="contact" data-field_name="whatsapp" value="<?= $modelContact->whatsapp; ?>" placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Viber:</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="viber" data-model_name="contact" data-field_name="viber" value="<?= $modelContact->viber; ?>" placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>
                        <!--User ID box ROW-->
                        <div class="id_user_box_text clf">
                            <p class="user_box_text clf fl">Skype:</p>
                            <div class="user_box_text_input clf fl">
                                <input type="text" name="skype" data-model_name="contact" data-field_name="skype" value="<?= $modelContact->skype; ?>" placeholder="" disabled="disabled">
                                <span class='dn_n hb edit_btn'>изменить</span>
                            </div>
                        </div>

                        <a class="change_btn dn_n fr hb clf" href="#change_btn">изменить</a>

                    </div>
                </div>
        </ul>
    </div>