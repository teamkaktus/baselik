<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\ProfileAsset;

ProfileAsset::register($this);
$this->title = 'Специализация';
?>

    <!--Pers.Cab Content Box-->
    <div class="pers_cab_box bw clf">
        <!-- Nav tabs -->
        <?= $this->render('menu'); ?>
        
        <ul id='nav_tabs_right' class="tab-content fr clf">
            <div role="tabpanel" class="tab-pane active" id="tid2">
                    <!--Spoiler Box-->
                    <div class="akord_main_box clf">
                        <!--Firhs Spoiler Box-->
                        
                        <?php foreach($resultCategory as $key1 => $category_1){ ?>
                        <div class="akord_box clf">
                            <p><?= $category_1['name']; ?><span class='art_box_open'></span></p>
                                <div class="akord_cont_box clf">
                                    <ul class="clf">
                                        <?php if($category_1 != ''){ ?>
                                            <?php $i = 0;?>
                                            <?php $length = count($category_1); ?>
                                            <?php foreach($category_1 as $key2 => $category_2){ ?>
                                            
                                                        <?php if(is_array($category_2)){ ?>
                                                            <?php if(($i == 0) || ($i % 3 == 0) ){ echo '<li>'; } ?>
                                                                <?php if(count($category_2) >= '2'){ ?>
                                                                    <div>
                                                                        <a><?= $category_2['name']; ?></a>
                                                                        <div class="hide_categori_box clf">
                                                                            <ul class="clf">
                                                                                <?php foreach($category_2 as $key3 => $category_3){ ?>
                                                                                    <?php if(is_array($category_3)){ ?>
                                                                                    <li>
                                                                                        <label class="categ_labl checkboxSpecialization" for="">
                                                                                            <input class="checkbox" <?php if(\Yii::$app->user->identity->categoryIsSelected($key3, \Yii::$app->user->id) == 'yes'){ echo 'checked="true"'; } ?>  data-category_id="<?= $key3; ?>" type="checkbox" name=""><?= $category_3['name']; ?> 
                                                                                        </label>
                                                                                    </li>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                <?php }else{ ?>
                                                                    <div>
                                                                        <a class="selectSpecialization <?php if(\Yii::$app->user->identity->categoryIsSelected($key2, \Yii::$app->user->id) == 'yes'){ echo 'choisen'; } ?>" data-category_id="<?= $key2; ?>"><?= $category_2['name']; ?></a>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php $i = $i+1; ?>
                                                            <?php if(($i % 3 == 0) || ($i == $length)){ echo '</li>'; } ?>
                                                        <?php } ?>
                                            <?php
                                            } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                            <?php
                            } 
                            ?>
                    </div>
            </div>
        </ul>
    </div>