<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="site-error">
    <img src="<?= Url::home().'img/ban.png'; ?>" style="display:block;margin:0 auto;margin-top:120px;max-width:515px;width:100%;" >
    <div style="display:block;margin:0 auto;margin-top:20px;max-width:515px;width:100%;">
        <h1 style="text-align: center;margin-bottom:0px !important;color:#91d97b;">Причина блокировки</h1>
        <p>
            <?= $modelUser->reason_ban; ?>
        </p>
    </div>
</div>
