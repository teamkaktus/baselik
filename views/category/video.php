<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\assets\VideoAsset;

VideoAsset::register($this);
$this->title = 'Видео';
?>
        <div class="breadcrumbMy">
            <ul class="breadcrumb"><li><a href="<?= Url::home(); ?>" style="color:black;">Home</a></li>
                <li class="active"><a href="<?= Url::home().'category/'.$categoryParent ?>" style="color:black;"><?= $modelCategory->name; ?></a></li>
            </ul>
        </div>
        <div class="serch_wr clf" style="position:relative;">
            <div class="serch_box bs clf">
                <?php $form = ActiveForm::begin(['action' => Url::home().'videosearch','method' => 'get']); ?>
                    <input AUTOCOMPLETE="off"  type="text" name="input_search" value="" placeholder="Поиск по специальности или услуге...">
                    <button class="clf fr"></button>
                <?php ActiveForm::end(); ?>
                <div class="box_category_list" style="z-index:999;border: 1px solid #b9b2ad;background-color:white;position:absolute;width:100%;">
                    <ul class="ul_category">
                    </ul>
                </div>
            </div>
            <p class="fl cb clf">Например: <span class="pruklad"><?= $arrayCategory[array_rand($arrayCategory,1)]; ?></span></p>
        </div>

        <div class="video_singl_box clf">
            <input type="hidden" class="GetCategory_id" value="<?= $modelVideo->category_id; ?>">
            <input type="hidden" class="without_video_id" value="<?= $modelVideo->video_src; ?>">
            <div class="video_singl_wrap clf">
                <iframe src="https://www.youtube.com/embed/<?= $modelVideo->video_src; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>

            <div class="vs_box_content clf">
                <div class="vs_box_content_Left fl clf">
                    <div class="vs_avatar fl clf">
                        
                        <?php 
                            if($modelUser->avatar == ''){
                                $avatar = 'img/default_avatar.png';
                            }else{
                                $avatar = 'img/users_images/'.$modelUser->avatar;
                            }
                        ?>
                        
                        <a href="#" style="padding-right:10px;">
                            <img src="<?= Url::home().$avatar; ?>" style="max-width:100px;min-width:100px;" alt="img">
                        </a>
                    </div>

                    <div class="vs_text_box clf">
                        <p class="vs_user_name">
                            <?php 
                                if($modelUser->name == ''){
                                    echo $modelUser->username;
                                }else{
                                    echo $modelUser->name;
                                }
                                if($modelUserVideosCount > 1){ 
                                    echo HTML::a('Все видео пользователя', Url::home().'allvideo/'.$modelUser->username,['style' => 'color:black;font-size:16px;padding-left:20px;']);
                                }
                            ?>
                        </p>
                        <p class="vs_user_jobs"><?= $modelCategory->name; ?></p>
                        <p class="vs_user_spec"><?= $modelVideo->description; ?></p>                        
                    </div>

                    <ul class="vs_box_top_nav clf">
                        <li><a class="vs_fav favotite" style="cursor:pointer;" data-video_id="<?= $modelVideo->id; ?>">Добавить в избранное</a></li>
                        <li>Опубликовано: <span id="date-created"></span>г.</li>
                        <li>Просмотрено: <span id="date-wived"></span></li>
                    </ul>
                
                    <div class="vs_spliter fl cb clf"></div>
                    <?php if(($modelUserContact!= null) && ($modelUserContact->time_work_start != '') && ($modelUserContact->time_work_end != '')){?>
                        <p class="vs_ansver_time fl cb clf">Время ответа на звонок с <?= $modelUserContact->time_work_start; ?> до <?= $modelUserContact->time_work_end; ?></p>
                    <?php } ?>
                 </div>

                <div class="vs_box_content_Right fr clf">
                    <a class="show_btn_box hb" href="#show_btn">Открыть контакт</a>

                    <div class="vs_box_hiden clf">
                        <ul class="clf">
                        <?php if($modelUserContact != null){ ?>
                            <?php if($modelUserContact->telefon != ''){ ?>
                                <li>
                                    <a href="tel:<?= $modelUserContact->telefon; ?>">
                                        <img src="../img/ico/h_phone.jpg" alt="img">
                                        <span><?= $modelUserContact->telefon; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                            <li>
                                <a href="mailto:<?= $modelUser->email; ?>">
                                    <img src="../img/ico/h_mail.jpg" alt="img">
                                    <span><?= $modelUser->email; ?></span>
                                </a>
                            </li>
                            <?php if($modelUserContact != null){ ?>
                                <?php if($modelUserContact->viber != ''){ ?>
                                    <li>
                                        <a href="#">
                                            <img src="../img/ico/h_vibr.jpg" alt="img">
                                            <span><?= $modelUserContact->viber; ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if($modelUserContact->whatsapp != ''){ ?>
                                    <li>
                                        <a href="#">
                                            <img src="../img/ico/h_vt.jpg" alt="img">
                                            <span><?= $modelUserContact->whatsapp; ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if($modelUserContact->skype != ''){ ?>
                                    <li>
                                        <a href="skype:valery">
                                            <img src="../img/ico/h_sky.jpg" alt="img">
                                            <span><?= $modelUserContact->skype; ?></span>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="video_box vb_fix clf">
                <p class="video_box_art clf cb">Другие исполнители</p>
            <div class="videoBoxContent">
            </div>
            <div class="col-sm-12">
                <a class="show_more_video_btn hb cb" href="#">Ещё</a>
            </div>
        </div>

