<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\assets\CategoryvideoAsset;

CategoryvideoAsset::register($this);
$this->title = 'Категории';
?>

        <div class="breadcrumbMy">
            <ul class="breadcrumb"><li><a href="<?= Url::home(); ?>" style="color:black;">Home</a></li>
                <li class="active"><?= $category_url?></li>
            </ul>
        </div>
        <div class="serch_wr clf" style="position:relative;">
            <div class="serch_box bs clf">
                <?php $form = ActiveForm::begin(['action' => Url::home().'videosearch','method' => 'get']); ?>
                    <input AUTOCOMPLETE="off"  type="text" name="input_search" value="" placeholder="Поиск по специальности или услуге...">
                    <button class="clf fr"></button>
                <?php ActiveForm::end(); ?>
                <div class="box_category_list" style="z-index:999;border: 1px solid #b9b2ad;background-color:white;position:absolute;width:100%;">
                    <ul class="ul_category">
                    </ul>
                </div>
            </div>
            <p class="fl cb clf">Например: <span class="pruklad"><?= $arrayCategory[array_rand($arrayCategory,1)]; ?></span></p>
        </div>
        <div class="profession_box clf">
            <ul class="clf">
                <?php 
                    $k = 0;
                    foreach($modelCategory as $category){
                    $k = $k + 1; ?>
                    <?php
                        if(Yii::$app->request->url == '/category/'.$category->url){
                            $class = 'cat_actv';
                        }else{
                            $class = '';
                        }
                    ?>
                    <li <?php if($k > 2){ echo 'style="display:none;"';} ?>>
                        <?= HTML::a($category->name,'/category/'.$category->url,['class' => $class,'data-category_id' => $category->id]); ?>
                    </li>
                <?php } ?>
            </ul>
            <a class="profession_more fl">Ещё профессии</a>
        </div>

        <!--Category Item Box-->       
        
        <div style="clear:both;"></div>


        <!--Category Content Box-->
        <div class="category_content clf">
            <!--Content Box Left-->
            <div class="cat_box_left fl">
                
                <ul class="clf">
                    <?php foreach($resultCategory as $key2 => $category_2){ ?>
                        <li class="categoryLi<?= $key2; ?>">
                           <?php if(is_array($category_2)){ ?>
                                <?php if(count($category_2) > 1){ ?>
                                    <a class="categoryClick" data-category_id="<?= $key2; ?>"><?= $category_2['name']; ?><span> 204</span></a>
                                    <ul class="hider_content_nav clf">
                                        <?php foreach($category_2 as $key3 => $category_3){ ?>
                                            <?php if(is_array($category_3)){ ?>
                                                <li>
                                                    <a class="categoryClick" data-category_id="<?= $key3; ?>"><?= $category_3['name']; ?><span> 204</span></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                <?php }else{ ?> 
                                    <a class="categoryClick" data-category_id="<?= $key2; ?>"><?= $category_2['name']; ?><span> 204</span></a>
                                <?php } ?>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>

            </div>


            <!--Content Box right-->
            <div class="cat_box_right fr">
                <!--Video Box-->
                <div id="vb3" class="video_box videoBoxContent clf" style="border-bottom: 0 !important;">
                
                </div>

                <div style="clear:both;"></div>
                <div class="col-sm-12">
                    <a class="show_more_video_btn hb cb" style="display:none;" href="#">Ещё</a>
                </div>
            </div>
        </div>
