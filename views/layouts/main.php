<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <div class="top_sb clf">
            <div class="top_sb_box clf">

                <div class="top_logo clf fl">
                    <a href="<?= Url::home(); ?>"><img src="../img/t_logo.png" alt="img"></a>
                </div>

                <div class="user_top_box clf fr">
                        <div class="sity_name clf fl">
                            <select class="selectCity">
                            </select>
                        </div>
                    <?php if(!\Yii::$app->user->isGuest){ ?>
                        <div class="top_authentication clf fr">
                            <div class="login_on_box clf fr">
                                <p>Здравствуйте, <a href="<?= Url::home(); ?>user/"><?= Yii::$app->user->identity->username; ?></a>!</p>
                                <?= Html::beginForm(['/site/logout'], 'post',['class'=>'pull-left']) ?>
                                    <?= HTML::a('<div class="countFavorite">0</div><img src="'.Url::home().'img/fav_star_header.png" style="width:20px;height:20px;" alt="img">',Url::home().'favorites',['class' => 'favoriteA config_box_mob fl']); ?>
                                    <!--<a class="config_box_mob fl" href="#"><img src="<?= Url::home(); ?>img/fav_star_header.png" style="width:20px;height:20px;" alt="img"></a>-->
                                    <?=  Html::submitButton(
                                        '<img src="'.Url::home().'img/exit_mob.png" alt="img">',
                                        ['class' => 'exit_box_mob fl','style' => 'outline: none;margin: 8px 0 0 9px;border: 1px solid transparent;background-color: transparent;']
                                    ); ?>
                                <?= Html::endForm(); ?>
                            </div>


                            <div class="box_auth_mob login_on clf fr">
                                <a class="config_box_mob fl" href="<?= Url::home(); ?>user/"><img src="<?= Url::home(); ?>img/config_mob.png" alt="img"></a>
                                <?= Html::beginForm(['/site/logout'], 'post',['class'=>'pull-left']) ?>
                                    <?= HTML::a('<div class="countFavorite">0</div><img src="'.Url::home().'img/fav_star_header.png" style="width:20px;height:20px;" alt="img">',Url::home().'favorites',['class' => 'favoriteA config_box_mob fl']); ?>
                                    <!--<a class="config_box_mob fl" href="#"><img src="<?= Url::home(); ?>img/fav_star_header.png" style="width:20px;height:20px;" alt="img"></a>-->
                                    <?=  Html::submitButton(
                                        '<img src="'.Url::home().'img/exit_mob.png" alt="img">',
                                        ['class' => 'exit_box_mob fl','style' => 'outline: none;margin: 8px 0 0 9px;border: 1px solid transparent;background-color: transparent;']
                                    ); ?>
                                <?= Html::endForm(); ?>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="top_authentication clf fr">
                            <?= HTML::a('<div class="countFavorite">0</div><img src="'.Url::home().'img/fav_star_header.png" style="width:20px;height:20px;" alt="img">',Url::home().'favorites',['class' => 'favoriteClass config_box_mob fl']); ?>
                            <a class="enter_box hb" href="#entr_mod">Вход</a>
                            <a class="reg_box hb" href="#reg_mod">Регистрация</a>

                            <div class="box_auth_mob clf fr">
                                <?= HTML::a('<div class="countFavorite">0</div><img src="'.Url::home().'img/fav_star_header.png" style="width:20px;height:20px;" alt="img">',Url::home().'favorites',['class' => 'favoriteClassM config_box_mob fl']); ?>
                                <a class="enter_box_mob fl" href="#entr_mod"><img src="../img/pass_mob.png" alt="img"></a>
                                <a class="reg_box_mob fl" href="#reg_mod"><img src="../img/login_mob.png" alt="img"></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
        
        
    
        <div style="display: none;">
            <div class="box-modal clf" id="entr_win">
                <div class="box-modal_close arcticmodal-close">X</div>
                <div class="up_win_art">
                    <p>Вход</p>
                </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => ''],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-1 control-label'],
                        ],
                    ]); ?>

                    <div class="up_win_input_box">
                        <input type="text" name="username" value="" placeholder="Логин">
                        <input type="password" name="password" value="" placeholder="Пароль">
                        <input type="hidden" name="rememberMe" value="1">
                        <?= HTML::a('Забыли пароль?',Url::home().'site/requestpasswordreset'); ?>
                    </div>

                    <?= Html::submitButton('Войти', ['class' => 'up_win_btn loginButton', 'name' => 'login-button']) ?>
                    <p class="up_win_reg_sn">Войти с помощью соцсетей:</p>
                    <?php echo \nodge\eauth\Widget::widget(array('action' => 'site/login')); ?>
                    <!-- <ul class="social_box clf">
                        <li><a target="_blank" href="#"><img src="../img/ico/vk.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/fb.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/tw.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/mr.jpg" alt="img"></a></li>
                    </ul> -->

                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11">
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <div style="display: none;">
            <div class="box-modal clf" id="upload_win">
                <?php $form = ActiveForm::begin([
                            'id' => 'signup-form',
                            'options' => ['class' => ''],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
                            ],
                        ]); ?>
                    <div class="box-modal_close arcticmodal-close">X</div>
                    <div class="up_win_art">
                        <p>Регистрация</p>
                    </div>

                    <div class="up_win_input_box">
                        <input type="text" name="username" value="" placeholder="Логин">
                        <input type="email" name="e_mail" value="" placeholder="E-mail">
                        <input type="password" name="password" value="" placeholder="Пароль">
                        <input type="password" name="confirm_password" value="" placeholder="Повторите пароль">
                        <div style="width:100%;line-height: 27px;margin-bottom:7px;">
                            <input type="checkbox" name="access_confirmation" class="form-control" value=""  style="display:inline-block;height:20px;width:20px;margin-left: 25px;float:left;">
                            <label class="label_access" style="margin-left: 5px;font-size:16px;float:left;">Принять соглашение</label>
                            <div style="clear:both;"></div>
                        </div>
                    </div>

                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'up_win_btn signupButton', 'name' => 'login-button']) ?>
                    <p class="up_win_reg_sn">Зарегистрироваться с помощью соцсетей:</p>
                    <?php echo \nodge\eauth\Widget::widget(array('action' => 'site/login')); ?>

                    <!-- <ul class="social_box clf">
                        <li><a target="_blank" href="#"><img src="../img/ico/vk.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/fb.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/tw.jpg" alt="img"></a></li>
                        <li><a target="_blank" href="#"><img src="../img/ico/mr.jpg" alt="img"></a></li>
                    </ul> -->
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php /*
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end(); */
    ?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
</div>

<div class="footer_wr">
    <div class="footer">
        <div class="fot_logo bs clf fl">
            <img src="../img/f_logo.png" alt="img">
        </div>

        <p class="all_right_mob">© 2016 | Все права защищены.</p>

        <ul class="fot_nav bs clf fl">
            <li>
                <?= HTML::a('О сервисе',Url::home().'site/about'); ?>
            </li>
            <li>
                <?= HTML::a('Вакансии',Url::home().'site/vakancui'); ?>
            </li>
            <li>
                <?= HTML::a('Контакты',Url::home().'site/contact'); ?>
            </li>
<!--            <li><a href="#">Работа</a></li>
            <li><a href="#">Исполнители</a></li>-->
        </ul>

        <div class="fot_copy clf fr">
            <p>Разработано в <a href="http://adtherapy.ru">Рекламотерапии</a></p>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
