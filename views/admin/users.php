<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\widgets\LinkPager;
    use app\assets\AdminAsset;
    AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Все пользователи'), '/admin',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
         
          <!-- Row Start -->
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Все пользователи
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="col-sm-12">
                                <table class="table">
                                    <tr>
                                        <td><b>#</b></td>
                                        <td><b>Имя Фамилия Отчество</b></td>
                                        <td><b>Логин</b></td>
                                        <td><b>email</b></td>
                                        <td></td>
                                    </tr>
                                    <?php foreach($modelUsers as $users){ ?>
                                        <tr>
                                            <td rowspan="2">
                                                <?php if($users->avatar != ''){ ?>
                                                    <img style="width:100px;" src="<?= Url::home().'img/users_images/'.$users->avatar; ?>">
                                                <?php }else{ ?>
                                                    <img style="width:100px;" src="<?= Url::home().'img/default_avatar.png'; ?>">
                                                <?php } ?>
                                            </td>
                                            <td><?= $users->name.' '.$users->lastname.' '.$users->lastname; ?></td>
                                            <td><?= $users->username; ?></td>
                                            <td><?= $users->email; ?></td>
                                            <td>
                                                <?php if($users->user_status == 1){ ?>
                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#users<?= $users->id; ?>">Забанить пользователя</button>
                                                <?php }else{ ?>
                                                    <?php $form = ActiveForm::begin(['id' => 'deactiveUser', 'options' => ['class' => 'form-horizontal']]); ?>
                                                            <input type="hidden" name="users_id" value="<?= $users->id; ?>">
                                                            <input type="hidden" name="type" value="unban">
                                                            <?= Html::submitButton('Разбанить пользователя', ['class' => 'btn btn-primary', 'name' => 'deactive']) ?>
                                                        <?php ActiveForm::end();  ?>
                                                <?php } ?>
                                            </td>
                                            <tr>
                                                <?php if($users->reason_ban != ''){?>
                                                    <td colspan="4">
                                                            <b>Причина бана:</b>
                                                            <?= $users->reason_ban; ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            
                                        </tr>
                                        
                                        <div class="modal fade" id="users<?= $users->id; ?>" role="dialog">       
                                            <div class="modal-dialog">
                                                <!-- Modal content -->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title" style='color:black;'>Забанить пользователя</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <?php $form = ActiveForm::begin(['id' => 'deactiveUser', 'options' => ['class' => 'form-horizontal']]); ?>
                                                            <input type="hidden" name="users_id" value="<?= $users->id; ?>">
                                                            <input type="hidden" name="type" value="ban">
                                                            <textarea rows="6" class="form-control" name="reason"></textarea>
                                                            <?= Html::submitButton('Забанить', ['class' => 'btn btn-primary', 'name' => 'deactive']) ?>
                                                        <?php ActiveForm::end();  ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="col-sm-12">
                                <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
            
          <!-- Row End -->

        </div>

      </div>
    </div>