<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\assets\AdminAsset;
use dosamigos\tinymce\TinyMce;
AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Страници'), '/admin/pages',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php 
                if(Yii::$app->session->hasFlash('save_page')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Страница добавлена',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('not_save_page')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Страница не добавлена'
                    ]);
                endif; 
            ?>
            
          <?php 
                if(Yii::$app->session->hasFlash('page_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Страница Обновлена',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('page_not_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Страница не обновлина'
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i> Добавить страницу
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <?php $form = ActiveForm::begin(['id' => 'addPage', 'options' => ['class' => 'form-horizontal']]); ?>
                                    <?= $form->field($newPage, 'url_page')->textInput()->label('SEO URL'); ?>
                                    <?= $form->field($newPage, 'title')->textInput()->label('Название страницы'); ?>
                                    <?= $form->field($newPage, 'content')->widget(TinyMce::className(), [
                                        'options' => ['rows' => 6,'id'=>'new'],
                                        'language' => 'ru',
//                                        'clientOptions' => [
//                                            'plugins' => [
//                                                "advlist autolink lists link charmap print preview anchor",
//                                                "searchreplace visualblocks code fullscreen",
//                                                "insertdatetime media table contextmenu paste"
//                                            ],
//                                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
//                                        ]
                                    ])->label('Контент страницы');?>
                                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'addPage']) ?>
                                <?php ActiveForm::end();  ?>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Все страницы
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <table class="table">
                                <tr>
                                    <td><b>#</b></td>
                                    <td><b>Название страницы</b></td>
                                    <td><b>Текст страницы</b></td>
                                    <td></td>
                                </tr>
                                <?php foreach($modelPages as $pages){ ?>
                                    <tr>
                                        <td><?= $pages->id; ?></td>
                                        <td><?= $pages->title; ?></td>
                                        <td><?= $pages->content; ?></td>
                                        <td><span  class="fa fa-edit" data-toggle="modal" data-target="#updatePage<?= $pages->id; ?>"></span></td>
                                    </tr>
                                    
                                    <div class="modal fade" id="updatePage<?= $pages->id; ?>" role="dialog">       
                                        <div class="modal-dialog" style="width:80%;">
                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title" style='color:black;'>Редактирование страницы</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php $form = ActiveForm::begin(['id' => 'addPage', 'options' => ['class' => 'form-horizontal']]); ?>
                                                        <?= $form->field($pages, 'id')->hiddenInput(['value' => $pages->id]); ?>
                                                        <?= $form->field($pages, 'title')->textInput()->label('Название страницы'); ?>
                                                        <?= $form->field($pages, 'content')->widget(TinyMce::className(), [
                                                                        'options' => ['rows' => 6,'id'=> $pages->id],
                                                                        'language' => 'es',
                                                                        'clientOptions' => [
                                                                            'plugins' => [
                                                                                "advlist autolink lists link charmap print preview anchor",
                                                                                "searchreplace visualblocks code fullscreen",
                                                                                "insertdatetime media table contextmenu paste"
                                                                ],
                                                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                                            ]
                                                        ])->label('Контент страницы'); ?>
                                                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'updatePages']) ?>
                                                    <?php ActiveForm::end();  ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

        </div>

      </div>
    </div>
