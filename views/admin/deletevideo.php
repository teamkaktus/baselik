<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Video;
use app\assets\AdmindeletevideoAsset;
error_reporting(E_ALL & ~E_NOTICE);

AdmindeletevideoAsset::register($this);


$storeFolder = \Yii::getAlias('@webroot');
require_once $storeFolder . '/src/config.php';

$oauthClientID = '952135163741-r00ams6ebtq4049v64i6m1k3dhp3a8pe.apps.googleusercontent.com';
$oauthClientSecret = '08GJANtyrlozTIBoZt6VWb0F';
$baseUri = 'http://'.$_SERVER['SERVER_NAME'];
$redirectUri = 'http://'.$_SERVER['SERVER_NAME'].'/admin/delete-video';

$showvideos = false;
//$redirectUri = \Yii::$app->request->referrer;
//return $this->redirect(Yii::$app->request->referrer);
//unset($_SESSION['token']);
define('OAUTH_CLIENT_ID', $oauthClientID);
define('OAUTH_CLIENT_SECRET', $oauthClientSecret);
define('REDIRECT_URI', $redirectUri);
define('BASE_URI', $baseUri);

//unset($_SESSION['token']);
$client = new Google_Client();
$client->setClientId(OAUTH_CLIENT_ID);
$client->setClientSecret(OAUTH_CLIENT_SECRET);
$client->setScopes('https://www.googleapis.com/auth/youtube');
$client->setRedirectUri(REDIRECT_URI);

$youtube = new Google_Service_YouTube($client);

$showForm = true;

if (isset($_GET['code'])) {
    if (strval($_SESSION['state']) !== strval($_GET['state'])) {
        die('The session state did not match.');
    }
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    return \Yii::$app->response->redirect(Url::to(['admin/delete-video']));
}

if (isset($_SESSION['token'])) {
    $client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken()) {
    
    try {
        $showvideos = true;
        if(isset($_POST['video_src']) && isset($_POST['video_id'])) {
            $delete = $youtube->videos->delete($_POST['video_src']);
            $modelVideoDelete = Video::find()->where(['id'=>$_POST['video_id']])->one();
            $modelVideoDelete->delete();
        }

    } catch (Google_ServiceException $e) {
        $htmlBodyNotice .= sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    } catch (Google_Exception $e) {
        $htmlBodyNotice .= sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        $htmlBodyNotice .= 'Please reset session';
        $showvideos = false;
    }

    $_SESSION['token'] = $client->getAccessToken();
}else{
    // If the user hasn't authorized the app, initiate the OAuth flow
    $state = mt_rand();
    $client->setState($state);
    $_SESSION['state'] = $state;

    $authUrl = $client->createAuthUrl();
    $htmlBody = <<<END
    <div class="video_box videoBoxContent clf" style="margin-bottom: 25px;">
        <h3>Требуется Авторизация</h3><br>
        <p>Вам нужно <a href="$authUrl" class="btn btn-danger">войти <i class="fa fa-youtube" aria-hidden="true"></i></a> перед загрузкой видео.<p>
    </div>
END;
}
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Загрузка на Youtube'), '/admin/', ['class' => 'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <!-- Row Start -->
            <div class="row wrap">
                <div class="video_box videoBoxContent clf">
                    <p class="reklab_art">Видео на удаление</p>
                    <?=$htmlBody;?>
                    <?php
                    if($showvideos) {
                        foreach ($modelVideo as $video) {
                            if(isset($_POST['video_id']) && $video['id'] == $_POST['video_id']){
                                continue;
                            }
                            ?>
                            <div class="video_item prem">
                                <img class="video_img" src="//img.youtube.com/vi/<?= $video['video_src']; ?>/0.jpg"
                                     alt="img">
                                <div style="text-align: left">
                                    <div style="display: inline-block; float: left;">
                                        <p class="vedeo_art"><?= $video['categoryName']; ?></p>
                                        <p class="vedeo_text"><?= $video['userName']; ?></p>
                                    </div>
                                    <?php $form = ActiveForm::begin(['id' => 'form-signup',
                                        'action' => Url::home().'admin/delete-video',
                                        'method' => 'POST',
                                        'options' => ['class' => 'form-horizontal'],
                                    ]); ?>
                                        <input type="hidden" name="video_src" value="<?= $video['video_src']; ?>">
                                        <input type="hidden" name="video_id" value="<?= $video['id']; ?>">
                                        <button class="btn btn-danger" style="float: right; margin: 11px 0px 0px;">Удалить
                                            <i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        <?php }
                    }
                    if(count($modelVideo) == 0 || count($modelVideo) == 1 && isset($_POST['video_id'])){
                        echo('<p class="bg-info" style="padding: 15px 0;">Сейчас нет видео для удаления</p>');
                    }
                    ?>

                </div>

            </div>
            <!-- Row End -->

        </div>

    </div>
</div>