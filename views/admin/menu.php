<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
    <?php    
    
        $class = ($this->context->getRoute() == 'admin/category')?'active':''; 
        $class1 = ($this->context->getRoute() == 'admin/video' || $this->context->getRoute() == 'admin/currentvideo')?'active':'';
        $class2 = ($this->context->getRoute() == 'admin/deletevideo')?'active':'';
        $class3 = ($this->context->getRoute() == 'admin/pages')?'active':''; 
        $class4 = ($this->context->getRoute() == 'admin/users')?'active':''; 
    ?>
    
    <ul>
        <li class="<?= $class; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-list"></i>Категории'), '/admin/category'); ?>
        </li>
        
        <li class="<?= $class1; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-file-video-o"></i>Модерация'), '/admin/video'); ?>
        </li>
        <li class="<?= $class2; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-youtube"></i>Удаление'), '/admin/delete-video'); ?>
        </li>
        <li class="<?= $class3; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-file-text"></i>Страницы'), '/admin/pages'); ?>
        </li>
        <li class="<?= $class4; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-users"></i>Пользователи'), '/admin/users'); ?>
        </li>
    </ul>
            
            
            
