<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AdminvideoAsset;

AdminvideoAsset::register($this);

$page_video = Url::to(['admin/currentvideo'], true);
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Модерация видео'), '/admin/', ['class' => 'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <!-- Row Start -->
            <div class="row wrap">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="video_box videoBoxContent clf">
                        <p class="reklab_art">Модерация видео</p>
                        <?php foreach ($modelVideos as $key => $video) { ?>

                            <div class="video_item prem" style="width: 292px;" id="video_<?=$video['id'];?>">
                                <video id="my_video_1" class="video-js vjs-default-skin" width="640px" height="267px"
                                       controls preload="none" poster='http://video-js.zencoder.com/oceans-clip.jpg'
                                       data-setup='{ "aspectRatio":"640:267", "playbackRates": [1, 1.5, 2] }'>
                                    <source src="<?= '/video/' . $video['video_src']; ?>" type='video/mp4'/>
                                </video>
                                <?php echo HTML::a(\Yii::t('app', 'Загрузить на <i class="fa fa-youtube" aria-hidden="true"></i>'), '/admin/currentvideo?video_id='.$video['id'], ['class' => 'UploadToYoutube btn btn-success']); ?>
                                <?php echo HTML::button(\Yii::t('app', 'Удалить <i class="fa fa-trash-o" aria-hidden="true"></i>'), ['class' => 'RemoveFile btn btn-danger', 'data-id' => $video['id']]); ?>
                            </div>

                        <?php }
                        if(count($modelVideos) <=0){
                            echo '<p class="bg-info" style="padding: 15px 0;">Сейчас нет видео на модерации</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- Row End -->

        </div>

    </div>
</div>

