<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdmincurrentvideoAsset;

error_reporting(E_ALL & ~E_NOTICE);
AdmincurrentvideoAsset::register($this);
$storeFolder = \Yii::getAlias('@webroot');
require_once $storeFolder . '/src/config.php';

$oauthClientID = '952135163741-r00ams6ebtq4049v64i6m1k3dhp3a8pe.apps.googleusercontent.com';
$oauthClientSecret = '08GJANtyrlozTIBoZt6VWb0F';
$baseUri = 'http://' . $_SERVER['SERVER_NAME'];
$redirectUri = 'http://' . $_SERVER['SERVER_NAME'] . '/admin/currentvideo';
//$redirectUri = \Yii::$app->request->referrer;
//return $this->redirect(Yii::$app->request->referrer);
//unset($_SESSION['token']);
define('OAUTH_CLIENT_ID', $oauthClientID);
define('OAUTH_CLIENT_SECRET', $oauthClientSecret);
define('REDIRECT_URI', $redirectUri);
define('BASE_URI', $baseUri);

//unset($_SESSION['token']);
$client = new Google_Client();
$client->setClientId(OAUTH_CLIENT_ID);
$client->setClientSecret(OAUTH_CLIENT_SECRET);
$client->setScopes('https://www.googleapis.com/auth/youtube');
$client->setRedirectUri(REDIRECT_URI);

$youtube = new Google_Service_YouTube($client);

$showForm = true;

if (isset($_GET['code'])) {
    if (strval($_SESSION['state']) !== strval($_GET['state'])) {
        die('The session state did not match.');
    }
    $client->authenticate($_GET['code']);
    $_SESSION['token'] = $client->getAccessToken();
    return \Yii::$app->response->redirect(Url::to(['admin/currentvideo']));
}

if (isset($_SESSION['token'])) {
    $client->setAccessToken($_SESSION['token']);
}


if (isset($_REQUEST['videoSubmit'])) {
    $videoTitle = $_REQUEST['title'];
    $videoDesc = $_REQUEST['description'];
    $playListId = $_REQUEST['playlistId'];
    if ($_REQUEST['video_src']) {
        $videoFilePath = $storeFolder . '/video/' . $_REQUEST['video_src'];

        if ($client->getAccessToken()) {
            try {
                $videoPath = $videoFilePath;

                $snippet = new Google_Service_YouTube_VideoSnippet();
                $snippet->setTitle($videoTitle);
                $snippet->setDescription($videoDesc);

                $snippet->setCategoryId("22");

                $status = new Google_Service_YouTube_VideoStatus();
                $status->privacyStatus = "public";

                $video = new Google_Service_YouTube_Video();
                $video->setSnippet($snippet);
                $video->setStatus($status);

                $chunkSizeBytes = 1 * 1024 * 1024;

                $client->setDefer(true);

                $insertRequest = $youtube->videos->insert("status,snippet", $video);

                $media = new Google_Http_MediaFileUpload(
                    $client,
                    $insertRequest,
                    'video/*',
                    null,
                    true,
                    $chunkSizeBytes
                );
                $media->setFileSize(filesize($videoPath));

                $status = false;
                $handle = fopen($videoPath, "rb");
                while (!$status && !feof($handle)) {
                    $chunk = fread($handle, $chunkSizeBytes);
                    $status = $media->nextChunk($chunk);
                }
                fclose($handle);

                $client->setDefer(false);

                @unlink($videoFilePath);


                $playlistId = $playListId;
                $resourceId = new Google_Service_YouTube_ResourceId();
                $resourceId->setVideoId($status['id']);
                $resourceId->setKind('youtube#video');

                $playlistItemSnippet = new Google_Service_YouTube_PlaylistItemSnippet();
                $playlistItemSnippet->setTitle('First video in the test playlist');
                $playlistItemSnippet->setPlaylistId($playlistId);
                $playlistItemSnippet->setResourceId($resourceId);

                $playlistItem = new Google_Service_YouTube_PlaylistItem();
                $playlistItem->setSnippet($playlistItemSnippet);
                $playlistItemResponse = $youtube->playlistItems->insert(
                    'snippet,contentDetails', $playlistItem, array());


                $model2Video->scenario = 'changeStatus';
                $model2Video->video_src = $status['id'];
                $model2Video->status = 1;
                $model2Video->save();

                setcookie("video_id", '');

                \Yii::$app->mailer->compose(['html' => 'videoUploaded-html'], ['username' => $modelVideo['userName'], 'name' => $modelVideo['name']])
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'baselik.ru'])
                    ->setTo($modelVideo['email'])
                    ->setSubject('baselik.ru - Модерация видео')
                    ->send();

                $showForm = false;
                $htmlBodySuccess .= "<div class=\"video_box videoBoxContent clf\">";
                $htmlBodySuccess .= "<p><h3>Видео успешно загружено.</h3></p><ul>";
                $htmlBodySuccess .= '<embed width="400" height="315" src="https://www.youtube.com/embed/' . $status['id'] . '"></embed>';
                $htmlBodySuccess .= '<li style="margin-top: 15px"><b>Название: </b>' . $status['snippet']['title'] . '</li>';
                $htmlBodySuccess .= '<li style="margin-top: 15px"><b>Описание: </b>' . $status['snippet']['description'] . '</li>';
                $htmlBodySuccess .= '<li style="margin-top: 15px"><a href="' . Url::home() . 'admin/video" class="btn btn-success">Назад ко всем видео</a></li>';
                $htmlBodySuccess .= '</ul>';
                $htmlBodySuccess .= '</div>';

            } catch (Google_ServiceException $e) {
                $htmlBodyNotice .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                    htmlspecialchars($e->getMessage()));
            } catch (Google_Exception $e) {
                $htmlBodyNotice .= sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                $htmlBodyNotice .= 'Please reset session <a href="logout.php">Logout</a>';
            }

            $_SESSION['token'] = $client->getAccessToken();
        }

    }
}
if ($client->getAccessToken()) {
    try {
        // var_dump('Stepan');exit;
        //header("Location: /admin/currentvideo");
        $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
            'mine' => 'true',
        ));

        $playlistItemsResponse = $youtube->playlists->listPlaylists('snippet', array(
            'channelId' => $channelsResponse->items[0]->id,
            'maxResults' => 50
        ));
        $options = '';
        foreach ($playlistItemsResponse->items as $item) {
            $options .= '<option value="' . $item['id'] . '">' . $item['snippet']['title'] . '</option>';
        }

    } catch (Google_ServiceException $e) {
        $htmlBodyNotice .= sprintf('<p>Сталася помилка служби: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    } catch (Google_Exception $e) {
        $htmlBodyNotice .= sprintf('<p>Сталася помилка служби: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
        $htmlBodyNotice .= 'Пожалуйста перезагрузите страницу';
        unset($_SESSION['token']);
        unset($_SESSION['state']);
        unset($_SESSION['google_data']); //Google session data unset
        $client->revokeToken();
        session_destroy();
        $showForm = false;
    }
}


//var_dump(file_get_contents('https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCkc-yzgnQXFRDRhxJelsyUQ&key=AIzaSyBJHF3o8WiIfGVUb1wmYJSpGVVHrHFq1PU'));

?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Загрузка на Youtube'), '/admin/', ['class' => 'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <!-- Row Start -->
            <div class="row wrap">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    $htmlBody = '';
                    echo $htmlBodySuccess;
                    echo $htmlBodyNotice;

                    // Check to ensure that the access token was successfully acquired.
                    if (!$client->getAccessToken()) {
                        // If the user hasn't authorized the app, initiate the OAuth flow
                        $state = mt_rand();
                        $client->setState($state);
                        $_SESSION['state'] = $state;

                        $authUrl = $client->createAuthUrl();
                        $htmlBody = <<<END
    <div class="video_box videoBoxContent clf">
	    <h3>Требуется Авторизация</h3><br>
	    <p>Вам нужно <a href="$authUrl" class="btn btn-danger">войти <i class="fa fa-youtube" aria-hidden="true"></i></a> перед загрузкой видео.<p>
	</div>
END;
                    }
                    ?>

                    <?php
                    echo $htmlBody;
                    if ($client->getAccessToken()) {
                        ?>
                        <div class="video_box videoBoxContent clf">
                            <?php if ($showForm) { ?>
                                <div class="form-group">
                                    <video id="my_video_1" class="video-js vjs-default-skin" width="640px"
                                           height="267px"
                                           controls preload="none" poster='http://video-js.zencoder.com/oceans-clip.jpg'
                                           data-setup='{ "aspectRatio":"640:267", "playbackRates": [1, 1.5, 2] }'>
                                        <source src="<?= '/video/' . $modelVideo['video_src']; ?>" type='video/mp4'/>
                                    </video>
                                </div>

                                <?php $form = ActiveForm::begin(['id' => 'form-signup',
                                    'action' => Url::home() . 'admin/currentvideo',
                                    'method' => 'POST',
                                    'options' => ['class' => 'form-horizontal'],
                                ]); ?>
                                <div class="form-group">
                                    <label for="title" class="control-label col-sm-2"> Название:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" id="title"
                                               value="<?= $modelVideo['userName'] . ' ' . $modelVideo['surname'] . ' - ' . $modelVideo['categoryName']; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label col-sm-2"> Описание:</label>
                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control" id="description" cols="20"
                                                  rows="2"><?= $modelVideo['description']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="control-label col-sm-2"> Плейлист:</label>
                                    <div class="col-sm-10">
                                        <select name="playlistId" id="playlistId" class="form-control">
                                            <?= $options; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="video_src" id="videoFile"
                                           value="<?= $modelVideo['video_src']; ?>">
                                    <input name="videoSubmit" class="btn btn-success" type="submit"
                                           value="Загрузить видео">
                                </div>
                                <?php ActiveForm::end();
                            } ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- Row End -->

        </div>

    </div>
</div>
