<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Админ панель';
?>
<div class="col-sm-offset-2 col-sm-8  col-md-6  col-lg-6 col-md-offset-3 col-lg-offset-3" style="margin-top: 50px">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-image: url('../img/pop_reg_bg.jpg');
    background-repeat: no-repeat;
    background-position: left top;background-size:cover;">
                <h1 class="panel-title" style="text-transform: uppercase;
    font: 38px 'BebasNeueRegular';color:white;"><i class="fa fa-lock"></i> Введите логин и пароль</h1>
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        ]); ?>

                        <?= $form->field($model, 'username')->textInput(['autofocus' => true,'style'=>'padding: 25px !important;'])->label('Логин'); ?>

                        <?= $form->field($model, 'password')->passwordInput(['style'=>'padding: 25px !important;'])->label('Пароль'); ?>

                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' => "<div class=\" col-lg-12\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->label('Запомнить меня') ?>

                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: center">
                                <?= Html::submitButton('Войти', ['class' => 'up_win_btn', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
</div>


</div>
