<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\assets\AdminAsset;
use dosamigos\tinymce\TinyMce;
AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Категории'), '/admin/category',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
         
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i>Добавить категорию
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <?php $form = ActiveForm::begin(['id' => 'addCategory', 'options' => ['class' => 'form-horizontal']]); ?>
                                    <?= $form->field($newCategory, 'name')->textInput()->label('Название категории'); ?>
                                    <?= $form->field($newCategory, 'url')->textInput()->label('SEO URL'); ?>
                                    <?= $form->field($newCategory, 'parent_id')->dropDownList($arrayCategory,['prompt' => ' -- Выберите категорию --'])->label('Родительская категория'); ?>
                                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'addCategory']) ?>
                                <?php ActiveForm::end();  ?>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Все категории
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <?php foreach($resultCategory as $key1 => $category_1){ ?>
                            <div class="akord_main_box clf">
                                <div class="akord_box clf">
                                    <p style="background-color:transparent;">
                                        <i id='category_name'><?= $category_1['name']; ?></i> 
                                        <span class='art_box_open' style="margin-right: 40px"></span>
                                        <form action='../admin/categorydelete/<?= $key1; ?>' style="">
                                            <i class="fa fa-pencil-square-o categoryUpdate pull-right"  id='categoryUpdate<?= $key1; ?>' data-type='parent' data-id='<?= $key1; ?>' style='padding-top: 1px;cursor:pointer;color:#5bc0de;'></i>
                                            <button type="submit" class="pull-right" id='categoryDelete<?= $key1; ?>' data-type='last' data-id='<?= $key1; ?>' style='border: 0px solid transparent;background-color: transparent;padding: 0px;cursor:pointer;color:#d9534f;'><span class="fa fa-times"></span></button>
                                        </form>
                                    </p>
                                    
                                    <?php //echo '-'.$key1.':'.$category_1['name'].'<br>';
                                        if($category_1 != ''){ ?>
                                            <div class="akord_cont_box clf">
                                                <?php foreach($category_1 as $key2 => $category_2){ ?>
                                                    <?php if(is_array($category_2)){ ?>
                                                        <table class='table table-hover' style='padding-bottom: 0px;margin-bottom: 0px'>
                                                            <tr>
                                                                <th style='padding-left: 30px'>
                                                                    <b>
                                                                        <span id='category_name'><?= $category_2['name']; ?></span>
                                                                        <form action='../admin/categorydelete/<?= $key2; ?>' style="display: inline-block;float:right;">
                                                                            <i class="fa fa-pencil-square-o pull-right categoryUpdate" id='categoryUpdate<?= $key2; ?>' data-type='first' data-id='<?= $key2; ?>' style='padding-top: 4px;cursor:pointer;color:#5bc0de;'></i>
                                                                            <button type="submit" class="pull-right"  id='categoryDelete<?= $key2; ?>' data-type='last' data-id='<?= $key2; ?>' style='border: 0px solid transparent;background-color: transparent;padding: 0px;cursor:pointer;color:#d9534f;'><span class="fa fa-times"></span></button>
                                                                        </form>
                                                                    </b>
                                                                </th>
                                                            </tr>
                                                            <?php foreach($category_2 as $key3 => $category_3){ ?>
                                                                <?php if(is_array($category_3)){ ?>
                                                                    <tr>
                                                                        <td style='padding-left: 50px'>
                                                                            <span id='category_name'><?= $category_3['name']; ?></span>
                                                                            <form action='../admin/categorydelete/<?= $key3; ?>' style="display: inline-block;float:right;">
                                                                                <i class="fa fa-pencil-square-o pull-right categoryUpdate"  id='categoryUpdate<?= $key3; ?>' data-type='last' data-id='<?= $key3; ?>' style='padding-top: 4px;cursor:pointer;color:#5bc0de;'></i>
                                                                                <button type="submit" class="pull-right"  id='categoryDelete<?= $key3; ?>' data-type='last' data-id='<?= $key3; ?>' style='border: 0px solid transparent;background-color: transparent;padding: 0px;cursor:pointer;color:#d9534f;'><span class="fa fa-times"></span></button>
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                               <?php } ?>
                                                        <?php } ?>
                                                        </table>
                                                    <?php } ?>
                                                <?php
                                                } ?>
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>
                            <?php
                            }
                            ?>           
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

        </div>

      </div>
    </div>

<div class="modal fade" id="updateCategoryBlock" role="dialog">       
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style='color:black;'>Редактирования категории</h4>
            </div>
            <div class="modal-body">
                <input type='text' class='form-control' id='inputUpdateCategory' data-category_id=''>
                <input type='hidden' id='inputUpdateCategoryId'>
                <input type='submit' class='btn btn-primary' id='saveUpdateCategory'>
            </div>
        </div>
    </div>
</div>