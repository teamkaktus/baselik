<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\City;
use app\models\Country;
use app\models\Video;
use app\models\Category;
use app\models\Usercategory;
use app\models\Usercontact;
use yii\web\HttpException;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect('/user/profile');
    }
    
    public function actionProfile()
    {
        if (!\Yii::$app->user->isGuest) {
                $user_id = \Yii::$app->user->id;
                $modelUser = User::find()->where(['id' => $user_id])->one();
            if(\Yii::$app->user->identity->user_status == 1) {
                $specializationCount = Usercategory::find()->where(['user_id' => $user_id])->count();

                return $this->render('profile',[
                    'modelUser' => $modelUser,
                    'specializationCount' => $specializationCount,
                ]);
            }else{
                return $this->render('ban',[
                    'modelUser' => $modelUser,
                ]);
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionSpecialization()
    {
        if (!\Yii::$app->user->isGuest) {
            $user_id = \Yii::$app->user->id;
            $modelUser = User::find()->where(['id' => $user_id])->one();
            if(\Yii::$app->user->identity->user_status == 1) {
                $modelCategory = Category::find()->all();

                $resultCategory = [];
                foreach($modelCategory as $parentCategory){
                    if(($parentCategory['parent_id'] == 0) || $parentCategory['parent_id']== null){
                        $resultCategory[$parentCategory['id']]['name'] = $parentCategory['name'];
                        foreach($modelCategory as $firstChild){
                            if($firstChild['parent_id'] == $parentCategory['id']){
                                $resultCategory[$parentCategory['id']][$firstChild['id']]['name'] = $firstChild['name'];
                                foreach($modelCategory as $lastChild){
                                    if($lastChild['parent_id'] == $firstChild['id']){
                                        $resultCategory[$parentCategory['id']][$firstChild['id']][$lastChild['id']]['name'] = $lastChild['name'];
                                    }
                                }
                            }
                        }                
                    }
                }

                return $this->render('specialization',[
                    'resultCategory' => $resultCategory,
                ]);
            }else{
                return $this->render('ban',[
                    'modelUser' => $modelUser,
                ]);
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionContact()
    {
        if (!\Yii::$app->user->isGuest) {
            $user_id = \Yii::$app->user->id;
            $modelUser = User::find()->where(['id' => $user_id])->one();
            if(\Yii::$app->user->identity->user_status == 1) {
                $modelContact = Usercontact::find()->where(['user_id'=>\Yii::$app->user->id])->one();
                $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
                if($modelContact == null){
                    $modelContact = new Usercontact;
                }
                return $this->render('contact',[
                    'modelContact' => $modelContact,
                    'modelUser' => $modelUser,
                ]);
            }else{
                return $this->render('ban',[
                    'modelUser' => $modelUser,
                ]);
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionVideo()
    {
        if (!\Yii::$app->user->isGuest) {
            $user_id = \Yii::$app->user->id;
            $modelUser = User::find()->where(['id' => $user_id])->one();
            if(\Yii::$app->user->identity->user_status == 1) {
                $query = new Query();
                $query->select(['*', 'id' => 'users_category.id'])
                                ->from('users_category')
                                ->join('LEFT JOIN',
                                                'category',
                                                'category.id = users_category.category_id'
                                        )->where(['users_category.user_id' => \Yii::$app->user->id]);

                $command = $query->createCommand();
                $modelCategoryChoisen = $command->queryAll();
                $arrayCategoryChoisen = ArrayHelper::map($modelCategoryChoisen, 'category_id', 'name');

                return $this->render('video',[
                    'arrayCategoryChoisen' => $arrayCategoryChoisen
                ]);
            }else{
                return $this->render('ban',[
                    'modelUser' => $modelUser,
                ]);
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionAllvideo($username=null)
    {
        if($username != null){
            $modelUser = User::find()->where(['username' => $username])->one();
            if($modelUser != null){
                $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
                return $this->render('allvideo',[
                    'arrayCategory' => $arrayCategory,
                    'username' => $username,
                ]);
            }else{
                throw new HttpException(404);
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionAjaxsavedataprofile()
    {
        if($_POST['model_name'] == 'contact'){
            $modelContact = Usercontact::find()->where(['user_id' => \Yii::$app->user->id])->one();
            if($modelContact == null){
                $modelContact = new Usercontact;
                $modelContact->user_id = \Yii::$app->user->id;
            }
            if($_POST['field_name'] != 'address_city'){
                $modelContact->$_POST['field_name'] = $_POST['new_value'];
                if($modelContact->save()){
                    $result['status'][] = 'success';
                }else{
                    $result['status'][] = 'error';
                }
            }else{
                $result['status'][] = 'address';
                $modelCity = City::find()->where(['city_name_ru' => $_POST['new_value']])->one();
                if($modelCity != null){
                    $modelContact->$_POST['field_name'] = $_POST['new_value'];
                    if($modelContact->save()){
                        $result['status_address'][] = 'good';
                    }else{
                        $result['status_address'][] = 'error';
                    }
                }else{
                    $result['status_address'][] = 'error';
                }
            }
            
        }elseif($_POST['model_name'] == 'profile'){
            if($_POST['field_name'] != 'password'){
                $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();

                $modelUser->scenario = 'update';
                $modelUser->$_POST['field_name'] = $_POST['new_value'];
                if($modelUser->save()){
                    $result['status'][] = 'success';
                }else{
                    $result['status'][] = 'error';
                }
            }else{
                $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
                $modelUser->scenario = 'change_password';
                $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($_POST['new_value']);
                
                if($modelUser->save()){
                    $result['status'][] = 'success';
                    $result['status_password'][] = 'success';
                }else{
                    $result['status'][] = 'error';
                    $result['status_password'][] = 'error';
                }
            }
            
        }
        echo json_encode($result);
        
    }
    
    public function actionAjaxcategoryuser()
    {
        $category_id = $_POST['category_id'];
        $type_request = $_POST['type_request'];
        $result = [];
        if($type_request == 'checked'){
            $modelUserCategory = new Usercategory();
            $modelUserCategory->user_id = \Yii::$app->user->id;
            $modelUserCategory->category_id = $category_id;
            if($modelUserCategory->save()){
                $result['status'][] = 'success';
            }else{
                $result['status'][] = 'error';
            }
        }else{
            $modelUserCategory = Usercategory::find()->where(['user_id' => \Yii::$app->user->id, 'category_id' => $category_id])->one();
            if($modelUserCategory != null){
                if($modelUserCategory->delete()){
                    $result['status'][] = 'success';
                }else{
                    $result['status'][] = 'error';
                }
            }else{
                $result['status'][] = 'error';
            }
        }
        echo json_encode($result);
    }
    
    public function actionAjaxvideoadd() {
        $result = [];
        $countSave = 0;
        foreach($_POST['arrayVideoForSave'] as $video){
            $newVideoModel = new Video();
            $newVideoModel->scenario = 'addVideo';
            $newVideoModel->user_id = \Yii::$app->user->id;
            $newVideoModel->category_id = $video['videoCategory'];
            $newVideoModel->video_src =  $video['videoName'];
            $newVideoModel->description =  $video['videoDescription'];
            $newVideoModel->status =  0;
            if($newVideoModel->save()){
                $countSave = $countSave + 1;
            }
        }
        if($countSave == count($_POST['arrayVideoForSave'])){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionGetprofilevideo(){
        
        $query = new Query;
        $query	->select(['*','id' => 'video.id','categoryName' => 'category.name','dateCreate' => 'video.date_create'])
                        ->from('video')
                        ->join(	'LEFT JOIN',
                            'users',
                            'users.id = video.user_id')
                        ->join(	'LEFT JOIN',
                            'category',
                            'category.id = video.category_id')
                ->where(['video.user_id' => \Yii::$app->user->id])->orderBy('video.date_create DESC');

        $command = $query->createCommand();
        $modelVideo = $command->queryAll();
        
        echo json_encode($modelVideo);
    }
    
    public function actionGetuserallvideo(){
        
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->join('LEFT JOIN',
                'category',
                'category.id = video.category_id')
            ->where(['users.username' => $_POST['username'], 'video.status' => 1])
            ->limit($limit)
            ->offset($offset);

        $command = $query->createCommand();
        $modelVideo = $command->queryAll();

        echo json_encode($modelVideo);
    }
    
    public function actionRemovevideouser(){
        $video_id = $_POST['video_id'];
        $modelVideo = Video::find()->where(['id'=>$_POST['video_id']])->one();
        if($modelVideo->status == 1){
            $modelVideo->scenario = 'remove_video_user';
            $modelVideo->status = '2';
            if($modelVideo->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }elseif(($modelVideo->status == 0) || ($modelVideo->status == 3)){
            if($modelVideo->delete()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }
        
        echo json_encode($result);
    }
    
    public function actionGetcity()
    {
        $city_name = $_POST['city_name'];
        $result = ArrayHelper::map(City::find()->where(['LIKE', 'city_name_ru', $city_name])->all(), 'id_city', 'city_name_ru');
        
        echo json_encode($result);
    }
    
}
