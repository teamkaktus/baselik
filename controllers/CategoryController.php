<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Query;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Usercontact;
use app\models\Video;
use yii\helpers\ArrayHelper;

class CategoryController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex($category_url)
    {
        $modelCategory = Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all();
        $modelCategoryChoise = Category::find()->all();

        $category_id = [];
        $resultCategory = [];
        foreach ($modelCategoryChoise as $parentCategory) {
            if ($parentCategory['url'] == $category_url) {
                $category_id[] = $parentCategory['id'];
                foreach ($modelCategoryChoise as $firstChild) {
                    if ($firstChild['parent_id'] == $parentCategory['id']) {
                        $category_id[] = $firstChild['id'];
                        $resultCategory[$firstChild['id']]['name'] = $firstChild['name'];
                        foreach ($modelCategoryChoise as $lastChild) {
                            if ($lastChild['parent_id'] == $firstChild['id']) {
                                $category_id[] = $lastChild['id'];
                                $resultCategory[$firstChild['id']][$lastChild['id']]['name'] = $lastChild['name'];
                            }
                        }
                    }
                }
            }
        }
        $modelVideo = Video::find()->where(['category_id' => $category_id, 'status' => '1'])->all();
        //var_dump($category_id);exit;
        $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        return $this->render('index', [
            'category_url' => $category_url,
            'modelCategory' => $modelCategory,
            'resultCategory' => $resultCategory,
            'modelVideo' => $modelVideo,
            'arrayCategory' => $arrayCategory,
        ]);
    }


    public function actionSavedropedfile()
    {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot') . '/video/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if (!is_dir($storeFolder . $ds . Yii::$app->user->id)) {
                mkdir($storeFolder . $ds . Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $fileType = $_FILES['file']['type'];    //3
            $arrayFormat = [
                'video/x-flv' => 'flv',
                'video/mp4' => 'mp4',
                'video/MP2T' => 'ts',
                'application/x-mpegURL' => 'm3u8',
                'video/3gpp' => '3gp',
                'video/quicktime' => 'mov',
                'video/x-msvideo' => 'avi',
                'video/x-ms-wmv' => 'wmv',
                'audio/x-m4a' => 'm4a',
            ];
            $keyStatus = array_key_exists($fileType, $arrayFormat);
            if ($keyStatus == true) {
                $fileType = $arrayFormat[$fileType];
            } else {
                $fileType = 'avi';
            }

            $targetPath = $storeFolder . $ds . Yii::$app->user->id . $ds;  //4
            $for_name = time();
            $targetFile = $targetPath . $for_name . '.' . $fileType;  //5

            move_uploaded_file($tempFile, $targetFile); //6
            return \Yii::$app->user->id . '/' . $for_name . '.' . $fileType;
        }

    }

    public function actionDeletefile()
    {
        $storeFolder = \Yii::getAlias('@webroot') . '/video/';
        $path = $storeFolder . $_POST['file_name'];
        if (unlink($path)) {
            echo 'delete';
        } else {
            echo 'problem';
        }
    }

    public function actionGetdatavideo()
    {
        $post_category_id = $_POST['category_id'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        //$modelCategory = Category::find()->where(['id' => $category_id])->all();
        $allCategory = Category::find()->all();
        $category_id = [];
        //$resultCategory = [];
        foreach ($allCategory as $parentCategory) {
            if ($parentCategory['id'] == $post_category_id) {
                $category_id[] = $parentCategory['id'];
                foreach ($allCategory as $firstChild) {
                    if ($firstChild['parent_id'] == $parentCategory['id']) {
                        $category_id[] = $firstChild['id'];
                        //$resultCategory[$firstChild['id']]['name'] = $firstChild['name'];
                        foreach ($allCategory as $lastChild) {
                            if ($lastChild['parent_id'] == $firstChild['id']) {
                                $category_id[] = $lastChild['id'];
                                // $resultCategory[$firstChild['id']][$lastChild['id']]['name'] = $lastChild['name'];
                            }
                        }
                    }
                }
            }
        }

        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->join('LEFT JOIN',
                'users_contact',
                'users_contact.user_id = video.user_id')
            ->join('LEFT JOIN',
                'category',
                'category.id = video.category_id')
            ->where(['category_id' => $category_id, 'status' => '1'])
            ->limit($limit)
            ->offset($offset);

        if(($_POST['cityFilter'] != '') && ($_POST['cityFilter'] != 'empty') && ($_POST['cityFilter'] != 'undefined')){
            $query->andWhere(['users_contact.address_city' => $_POST['cityFilter']]);
        }
        
        $command = $query->createCommand();
        $modelVideo = $command->queryAll();

        //$modelVideo = Video::find()->where(['category_id'=>$category_id,'status'=>'1'])->all();
        //var_dump($modelVideo);

        echo json_encode($modelVideo);
    }

    public function actionGethomevideo()
    {
        if (isset($_POST['limit']) && isset($_POST['offset'])) {
            $query = new Query;
            $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
                ->from('video')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = video.user_id')
                ->join('LEFT JOIN',
                    'users_contact',
                    'users_contact.user_id = video.user_id')
                ->join('LEFT JOIN',
                    'category',
                    'category.id = video.category_id')
                ->where(['status' => 1])
                ->limit($_POST['limit'])
                ->offset($_POST['offset']);
            if(($_POST['cityFilter'] != '') && ($_POST['cityFilter'] != 'empty') && ($_POST['cityFilter'] != 'undefined')){
                $query->andWhere(['users_contact.address_city' => $_POST['cityFilter']]);
            }
            $command = $query->createCommand();
            $modelVideo = $command->queryAll();
        } else {
            $modelVideo = null;
        }

        echo json_encode($modelVideo);
    }

    public function actionVideo($video_id)
    {

        $modelVideo = Video::find()->where(['id' => $video_id])->one();
        $modelUser = User::find()->where(['id' => $modelVideo->user_id])->one();
        $modelCategory = Category::find()->where(['id' => $modelVideo->category_id])->one();
        $modelUserContact = Usercontact::find()->where(['user_id' => $modelVideo->user_id])->one();
        $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        $modelUserVideosCount = Video::find()->where(['user_id' => $modelVideo->user_id, 'status' => 1])->count(); 
        
        $categoryParent = $modelCategory->url;
        $modelParent = Category::find()->where(['id'=>$modelCategory->parent_id])->one();
        
        if($modelParent != null){
            $categoryParent = $modelParent->url;
            $modelParent_2 = Category::find()->where(['id'=>$modelParent->parent_id])->one();
            if($modelParent_2 != null){
                $categoryParent = $modelParent_2->url;
            }
        }
        
        
        return $this->render('video', [
            'modelVideo' => $modelVideo,
            'modelCategory' => $modelCategory,
            'modelUserContact' => $modelUserContact,
            'modelUser' => $modelUser,
            'arrayCategory' => $arrayCategory,
            'categoryParent' => $categoryParent,
            'modelUserVideosCount' => $modelUserVideosCount
        ]);
    }

    public function actionFavorites()
    {
        $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        return $this->render('favorites', [
            'arrayCategory' => $arrayCategory
        ]);
    }

    public function actionGetfavoritevideo()
    {
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->join('LEFT JOIN',
                'category',
                'category.id = video.category_id')
            ->where(['video.id' => $_POST['arrayVideo'], 'status' => 1])
            ->limit($limit)
            ->offset($offset);

        $command = $query->createCommand();
        $modelVideo = $command->queryAll();

        echo json_encode($modelVideo);
    }

    public function actionUploadavatar()
    {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot') . '/img/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if (!is_dir($storeFolder . $ds . Yii::$app->user->id)) {
                mkdir($storeFolder . $ds . Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3

            $targetPath = $storeFolder . $ds . Yii::$app->user->id . $ds;  //4

            $for_name = time();

            $targetFile = $targetPath . $for_name . '.jpg';  //5

            move_uploaded_file($tempFile, $targetFile); //6
            return \Yii::$app->user->id . '/' . $for_name . '.jpg';
        }
    }

    public function actionDeleteavatar()
    {
        $storeFolder = \Yii::getAlias('@webroot') . '/img/users_images/';
        $path = $storeFolder . $_POST['file_name'];
        if (unlink($path)) {
            echo 'delete';
        } else {
            echo 'problem';
        }
    }

    public function actionAddavatar()
    {
        $avatar_src = $_POST['avatar_src'];
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $modelUser->scenario = 'change_avatar';
        $modelUser->avatar = $avatar_src;
        if ($modelUser->save()) {
            $result = 'save';
        } else {
            $result = 'not_save';
        }
        echo json_encode($result);
    }

    public function actionGeteanothervideos()
    {
        $category_id = $_POST['category_id'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->join('LEFT JOIN',
                'category',
                'category.id = video.category_id')
            ->where(['video.category_id' => $category_id, 'status' => 1])->andWhere(['not', ['video.id' => $_POST['without_video_id']]])
            ->limit($limit)
            ->offset($offset);

        $command = $query->createCommand();
        $modelVideo = $command->queryAll();

        echo json_encode($modelVideo);
    }

    public function actionGetcategoryname()
    {
        $text = $_POST['text'];
        $arrayCategory = ArrayHelper::map(Category::find()->where(['LIKE', 'name', $text])->all(), 'id', 'name');
        $result = '';
        $result['data'] = $arrayCategory;
        echo json_encode($result);
    }

    public function actionGetsearchvideo()
    {
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $text = $_POST['text'];
        $arrayCategory = ArrayHelper::map(Category::find()->where(['LIKE', 'name', $text])->all(), 'id', 'name');
        if ($arrayCategory != null) {
            $category_id = [];
            foreach ($arrayCategory as $key => $category_name) {
                $category_id[] = $key;
            }

            $query = new Query;
            $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
                ->from('video')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = video.user_id')
                ->join('LEFT JOIN',
                    'users_contact',
                    'users_contact.user_id = video.user_id')
                ->join('LEFT JOIN',
                    'category',
                    'category.id = video.category_id')
                ->where(['video.category_id' => $category_id, 'status' => 1])
                ->limit($limit)
                ->offset($offset);

            if(($_POST['cityFilter'] != '') && ($_POST['cityFilter'] != 'empty') && ($_POST['cityFilter'] != 'undefined')){
                $query->andWhere(['users_contact.address_city' => $_POST['cityFilter']]);
            }
            
            $command = $query->createCommand();
            $modelVideo = $command->queryAll();
        } else {
            $modelVideo = null;
        }


        echo json_encode($modelVideo);
    }
}
