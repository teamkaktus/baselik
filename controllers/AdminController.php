<?php

namespace app\controllers;

use app\models\Video;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Pages;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\web\HttpException;
error_reporting(E_ALL & ~E_NOTICE);

class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            if (\Yii::$app->user->identity->users_type != 'admin') {
                return $this->goHome();
            } else {
                return $this->redirect('/admin/category');
            }
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/category');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionPages()
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
            $request = Yii::$app->request;
            $modelNewPage = new Pages();
            $modelNewPage->scenario = 'addPage';

            if($_POST){
                if(isset($_POST['updatePages'])){
                    $modelPageU = Pages::find()->where(['id' => $_POST['Pages']['id']])->one();
                    $modelPageU->scenario = 'updatePage';
                    $modelPageU->content = $_POST['Pages']['content'];
                    if($modelPageU->save()){
                        Yii::$app->session->setFlash('page_update');
                    }else{
                        Yii::$app->session->setFlash('page_not_update');
                    }
                }elseif(isset($_POST['addPage'])){
                    if ($modelNewPage->load($request->post())) {
                        if($modelNewPage->save()){
                            Yii::$app->session->setFlash('save_page');
                        }else{
                            Yii::$app->session->setFlash('not_save_page');
                        }
                    }
                }
            }

            $modelPages = Pages::find()->all();
            return $this->render('pages',[
                'newPage' => $modelNewPage,
                'modelPages' => $modelPages
            ]);
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionCategory()
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
                $newCategory = new Category();
                $newCategory->scenario = 'addCategory';

                if ($_POST) {
                    $request = Yii::$app->request;
                    if ($newCategory->load($request->post())) {
                        $newCategory->save();
                    }
                }
                $modelCategory = Category::find()->all();

                $resultCategory = [];
                foreach ($modelCategory as $parentCategory) {
                    if (($parentCategory['parent_id'] == 0) || $parentCategory['parent_id'] == null) {
                        $resultCategory[$parentCategory['id']]['name'] = $parentCategory['name'];
                        foreach ($modelCategory as $firstChild) {
                            if ($firstChild['parent_id'] == $parentCategory['id']) {
                                $resultCategory[$parentCategory['id']][$firstChild['id']]['name'] = $firstChild['name'];
                                foreach ($modelCategory as $lastChild) {
                                    if ($lastChild['parent_id'] == $firstChild['id']) {
                                        $resultCategory[$parentCategory['id']][$firstChild['id']][$lastChild['id']]['name'] = $lastChild['name'];
                                    }
                                }
                            }
                        }
                    }
                }

                $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');

                return $this->render('category', [
                    'newCategory' => $newCategory,
                    'arrayCategory' => $arrayCategory,
                    'resultCategory' => $resultCategory,
                ]);
            
        }else{
            throw new HttpException(404);
        }
    }

    public function actionAjaxcategoryupdate()
    {
        $category_name = $_POST['category_name'];
        $category_id = $_POST['category_id'];

        $modelCategory = Category::find()->where(['id' => $category_id])->one();
        $modelCategory->scenario = 'updateCategory';
        $modelCategory->name = $category_name;
        $result = [];
        if ($modelCategory->save()) {
            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }

    public function actionCategorydelete($category_id = null)
    {

        $modelCategory = Category::find()->where(['id' => $category_id])->one();

        $result = [];
        if ($modelCategory->delete()) {
            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }
        return $this->redirect('../../../admin/category');
    }

    public function actionVideo()
    {
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
            $query = new Query;
            $query->select(['*'])
                ->from('video')
                ->where(['status' => 0]);

            $command = $query->createCommand();
            $modelVideos = $command->queryAll();
            return $this->render('video', [
                'modelVideos' => $modelVideos
            ]);
        }else{
            throw new HttpException(404);
        }
    }

    public function actionGetvideoserver()
    {
        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->join('LEFT JOIN',
                'category',
                'category.id = video.category_id')
            ->where(['status' => 0]);

        $command = $query->createCommand();
        $modelVideo = $command->queryAll();

        echo json_encode($modelVideo);
    }

    public function actionUploadyoutube()
    {
        //google_api_php_client_autoload();
        $oauthClientID = '952135163741-r00ams6ebtq4049v64i6m1k3dhp3a8pe.apps.googleusercontent.com';
        $oauthClientSecret = '08GJANtyrlozTIBoZt6VWb0F';
        $baseUri = 'http://youtube-upload.dev/';
        $redirectUri = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'], FILTER_SANITIZE_URL);

        define('OAUTH_CLIENT_ID',$oauthClientID);
        define('OAUTH_CLIENT_SECRET',$oauthClientSecret);
        define('REDIRECT_URI',$redirectUri);
        define('BASE_URI',$baseUri);

        session_start();

        $client = new Google_Client();
        $client->setClientId(OAUTH_CLIENT_ID);
        $client->setClientSecret(OAUTH_CLIENT_SECRET);
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->setRedirectUri(REDIRECT_URI);

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        $videoFilePath = '';
        $videoTitle = '';
        $videoDesc = '';
        $videoTags = '';
        if(isset($_REQUEST['videoSubmit'])){
            $videoTitle = $_REQUEST['title'];
            $videoDesc = $_REQUEST['description'];
            $videoTags = $_REQUEST['tags'];

            $ds           = DIRECTORY_SEPARATOR;
            $storeFolder  = \Yii::getAlias('@webroot');
            $videoFilePath = $storeFolder.$ds.'video'.$ds.$_REQUEST['video_src'];

            var_dump($videoFilePath);
            exit;
        }




        /*
         * You can acquire an OAuth 2.0 client ID and client secret from the
         * Google Developers Console <https://console.developers.google.com/>
         * For more information about using OAuth 2.0 to access Google APIs, please see:
         * <https://developers.google.com/youtube/v3/guides/authentication>
         * Please ensure that you have enabled the YouTube Data API for your project.
         */



        if (isset($_GET['code'])) {
            if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                die('The session state did not match.');
            }

            $client->authenticate($_GET['code']);
            $_SESSION['token'] = $client->getAccessToken();

            header('Location: ' . REDIRECT_URI);
        }

        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
        }

        $htmlBody = '';

        var_dump($videoFilePath);
// Check to ensure that the access token was successfully acquired.
        if ($client->getAccessToken()) {
            try{
                // REPLACE this value with the path to the file you are uploading.
                $videoPath = $videoFilePath;

                // Create a snippet with title, description, tags and category ID
                // Create an asset resource and set its snippet metadata and type.
                // This example sets the video's title, description, keyword tags, and
                // video category.
                $snippet = new Google_Service_YouTube_VideoSnippet();
                $snippet->setTitle($videoTitle);
                $snippet->setDescription($videoDesc);
                $snippet->setTags(explode(",",$videoTags));

                // Numeric video category. See
                // https://developers.google.com/youtube/v3/docs/videoCategories/list
                $snippet->setCategoryId("22");

                // Set the video's status to "public". Valid statuses are "public",
                // "private" and "unlisted".
                $status = new Google_Service_YouTube_VideoStatus();
                $status->privacyStatus = "public";

                // Associate the snippet and status objects with a new video resource.
                $video = new Google_Service_YouTube_Video();
                $video->setSnippet($snippet);
                $video->setStatus($status);

                // Specify the size of each chunk of data, in bytes. Set a higher value for
                // reliable connection as fewer chunks lead to faster uploads. Set a lower
                // value for better recovery on less reliable connections.
                $chunkSizeBytes = 1 * 1024 * 1024;

                // Setting the defer flag to true tells the client to return a request which can be called
                // with ->execute(); instead of making the API call immediately.
                $client->setDefer(true);

                // Create a request for the API's videos.insert method to create and upload the video.
                $insertRequest = $youtube->videos->insert("status,snippet", $video);

                // Create a MediaFileUpload object for resumable uploads.
                $media = new Google_Http_MediaFileUpload(
                    $client,
                    $insertRequest,
                    'video/*',
                    null,
                    true,
                    $chunkSizeBytes
                );
                $media->setFileSize(filesize($videoPath));

                // Read the media file and upload it.
                $status = false;
                $handle = fopen($videoPath, "rb");
                while (!$status && !feof($handle)) {
                    $chunk = fread($handle, $chunkSizeBytes);
                    $status = $media->nextChunk($chunk);
                }
                fclose($handle);

                // If you want to make other calls after the file upload, set setDefer back to false
                $client->setDefer(false);

                // Update youtube video ID to database
                //$db->update($result['video_id'],$status['id']); *******************************************************
                // delete video file from local folder
                @unlink($videoFilePath);

                echo('video uploaded');
                exit;

            } catch (Google_ServiceException $e) {
                /*$htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
                    htmlspecialchars();*/
                echo $e->getMessage();
            } catch (Google_Exception $e) {
                /*$htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
                $htmlBody .= 'Please reset session <a href="logout.php">Logout</a>';*/
                echo $e->getMessage();
            }

            $_SESSION['token'] = $client->getAccessToken();
        } else {
            // If the user hasn't authorized the app, initiate the OAuth flow
            $state = mt_rand();
            $client->setState($state);
            $_SESSION['state'] = $state;

            /*$authUrl = $client->createAuthUrl();
            $htmlBody = <<<END
	        <h3>Authorization Required</h3>
	        <p>You need to <a href="$authUrl">authorize access</a> before proceeding.<p>
            END;*/
        }
    }

    public function actionCurrentvideo(){
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
            if($_GET['video_id']) {
                setcookie ("video_id", $_GET['video_id']);
                $video_id = $_GET['video_id'];

                $query = new Query;
                $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name'])
                    ->from('video')
                    ->join('LEFT JOIN',
                        'category',
                        'category.id = video.category_id')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = video.user_id')
                    ->where(['video.id' => $video_id]);

                $command = $query->createCommand();
                $modelVideo = $command->queryOne();

                $model2Video = Video::find()->where(['id' => $video_id])->one();
                
                return $this->render('currentvideo', [
                    'modelVideo' => $modelVideo,
                    'model2Video' => $model2Video
                ]);
            }elseif($_COOKIE["video_id"]){

                $video_id = $_COOKIE["video_id"];

                $query = new Query;
                $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name', 'userName' => 'users.name'])
                    ->from('video')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = video.user_id')
                    ->join('LEFT JOIN',
                        'category',
                        'category.id = video.category_id')
                    ->where(['video.id' => $video_id]);

                $command = $query->createCommand();
                $modelVideo = $command->queryOne();

                $model2Video = Video::find()->where(['id' => $video_id])->one();

                return $this->render('currentvideo', [
                    'modelVideo' => $modelVideo,
                    'model2Video' => $model2Video,
                ]);
            }else{
                return $this->redirect('/admin/video');
            }
        }else{
            throw new HttpException(404);
        }
    }

    public function actionRemovevideo($video_id){
        $storeFolder = \Yii::getAlias('@webroot');
        $modelVideo = Video::find()->where(['id' => $video_id])->one();

        echo json_encode($modelVideo->id);
        $videoFilePath = $storeFolder.'/video/'.$modelVideo->video_src;

        $modelVideo->scenario = 'changeStatus';
        $modelVideo->video_src = '';
        $modelVideo->status_description = $_POST['status_desc'];
        $modelVideo->status = 3;
        $modelVideo->save();

        $query = new Query;
        $query->select(['*', 'id' => 'video.id', 'userName' => 'users.name'])
            ->from('video')
            ->join('LEFT JOIN',
                'users',
                'users.id = video.user_id')
            ->where(['video.id' => $video_id]);

        $command = $query->createCommand();
        $model2Video = $command->queryOne();

        \Yii::$app->mailer->compose(['html' => 'deledeVideo-html'], ['name' => $model2Video['userName'], 'status_description' => $_POST['status_desc']])
            ->setFrom([\Yii::$app->params['supportEmail'] => 'baselik.ru'])
            ->setTo($model2Video['email'])
            ->setSubject('baselik.ru - Модерация видео')
            ->send();

        @unlink($videoFilePath);
    }

    public function actionDeletevideo(){
        
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
            $query = new Query;
            $query->select(['*', 'id' => 'video.id', 'categoryName' => 'category.name', 'userName' => 'users.name'])
                ->from('video')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = video.user_id')
                ->join('LEFT JOIN',
                    'category',
                    'category.id = video.category_id')
                ->where(['video.status' => 2]);

            $command = $query->createCommand();
            $modelVideo = $command->queryAll();

            return $this->render('deletevideo', [
                'modelVideo' => $modelVideo
            ]);
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionUsers(){
        if (!\Yii::$app->user->isGuest && (\Yii::$app->user->identity->users_type == 'admin')) {
            if($_POST){
                if($_POST['type']== 'ban'){
                    $modelUser = User::find()->where(['id' => $_POST['users_id']])->one();
                    $modelUser->scenario = 'ban';
                    $modelUser->reason_ban = $_POST['reason'];
                    $modelUser->user_status = 0;
                    if($modelUser->save()){
                        $modelVideos = Video::find()->where(['user_id' => $_POST['users_id'],'status' => 1])->all();
                        foreach($modelVideos as $video){
                            $modelVideo = Video::find()->where(['id' => $video->id])->one();
                            $modelVideo->scenario = 'remove_video_user';
                            $modelVideo->status = 4;
                            $modelVideo->save();
                        }
                    }
                    //var_dump($_POST);exit;
                }elseif($_POST['type']== 'unban'){
                    $modelUser = User::find()->where(['id' => $_POST['users_id']])->one();
                    $modelUser->scenario = 'ban';
                    $modelUser->reason_ban = '';
                    $modelUser->user_status = 1;
                    if($modelUser->save()){
                        $modelVideos = Video::find()->where(['user_id' => $_POST['users_id'],'status' => 4])->all();
                        foreach($modelVideos as $video){
                            $modelVideo = Video::find()->where(['id' => $video->id])->one();
                            $modelVideo->scenario = 'remove_video_user';
                            $modelVideo->status = 1;
                            $modelVideo->save();
                        }
                    }
                }
            }
            
            $queryUser = User::find()->andWhere(['not', ['users_type' => 'admin']]);
            $modelUsers = new ActiveDataProvider(['query' => $queryUser, 'pagination' => ['pageSize' => 20]]);
            return $this->render('users',[
                'modelUsers' => $modelUsers->getModels(),
                'pagination' => $modelUsers->pagination,
                'count' => $modelUsers->pagination->totalCount,
            ]);
        }else{
            throw new HttpException(404);
        }
    }

}
