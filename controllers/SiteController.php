<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Pages;
use app\models\Category;
use app\models\Usercontact;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\lib\LImageHandler;

error_reporting(E_ALL & ~E_NOTICE);

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('login'),
            ),
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $modelCategory = Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all();
        $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        $userCount = User::find()->count();
        return $this->render('index',[
            'modelCategory' => $modelCategory,
            'arrayCategory' => $arrayCategory,
            'userCount' => $userCount
        ]);
    }

    public function actionAjaxlogin(){
//        if (!\Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
        $login = [];
        $login['LoginForm']['username'] = $_POST['username'];
        $login['LoginForm']['password'] = $_POST['password'];
        $login['LoginForm']['rememberMe'] = '1';

        $result = [];
        $model = new LoginForm();
        if ($model->load($login) && $model->login()) {
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }

    public function actionAjaxsignup(){
        $modelUser = new User();
        $modelUser->scenario = 'registration';

        $data['User']['username'] = $_POST['username'];
        $data['User']['password'] = $_POST['password'];
        $data['User']['email'] = $_POST['e_mail'];

        $result = [];
        $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($_POST['password']);
        if ($modelUser->load($data) && $modelUser->save()){
            if (Yii::$app->getUser()->login($modelUser)){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);

    }

    public function actionLogin()
    {
      $serviceName = Yii::$app->getRequest()->getQueryParam('service');
      if (isset($serviceName)) {
      /** @var $eauth \nodge\eauth\ServiceBase */
      $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
      $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
      $cancelUrl = str_replace('//', '/', Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));
      $cancelUrl = str_replace(':/', '://', $cancelUrl);
      $eauth->setCancelUrl($cancelUrl);
      try {
        if ($eauth->authenticate()) {
          $profile = $eauth->getAttributes();
          $model = new User();
          if(!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id'], 'users_type' => $eauth->getServiceName()])->exists()){
            $model->scenario = 'social_login';
            $model->password = \Yii::$app->security->generateRandomString(8);
            $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = \Yii::$app->security->generateRandomString(8);
            $model->username = $profile['name'];
            $model->email = (isset($profile['email']))?$profile['email']:$profile['id'];
            $model->gender = ((isset($profile['gender'])&&(($profile['gender'] == 'male')||(($profile['gender'] == 'M')))))?'1':'0';
            $model->name = (isset($profile['first_name']))?$profile['first_name']:'';
            $model->lastname = (isset($profile['last_name']))?$profile['last_name']:'';
            $model->users_type = $eauth->getServiceName();
            if ($model->save()) {
              if (Yii::$app->getUser()->login($model)) {
                $eauth->redirect();
              }
            }
          }else {
            $model = User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->one();
            if (Yii::$app->getUser()->login($model)) {
              $eauth->redirect();
            }
          };
          exit;

          // special redirect with closing popup window
        }
        else {
          // close popup window and redirect to cancelUrl
          $eauth->cancel();
        }
      }
      catch (\nodge\eauth\ErrorException $e) {
        // save error to show it later
        Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
        // close popup window and redirect to cancelUrl
        //              $eauth->cancel();
        $eauth->redirect($eauth->getCancelUrl());
      }
      }
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if($_POST){
            //var_dump(Yii::$app->request->post());exit;
        }
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
        $modelPage = Pages::find()->where(['url_page' => 'site/contact'])->one();
        return $this->render('contact',[
            'content' => $modelPage->content,
        ]);
    }

    public function actionSignup(){
        $model = new User();

        $request = Yii::$app->request;
        $postedForm = $request->post('ContactForm');

        $model->scenario = 'registration';
        if ($model->load($request->post())){
            $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = 'key';
            if ($model->save()) {
                    if (Yii::$app->getUser()->login($model)) {
                            return $this->goHome();
                    }
            }
	}

        return $this->render('signup', [
            'newModel' => $model,
        ]);
    }

    public function actionAbout()
    {
        $modelPage = Pages::find()->where(['url_page' => 'site/about'])->one();
        return $this->render('about',[
            'content' => $modelPage->content,
        ]);
    }

    public function actionVakancui()
    {
        $modelPage = Pages::find()->where(['url_page' => 'site/vakancui'])->one();
        return $this->render('vakancui',[
            'content' => $modelPage->content,
        ]);
    }

    public function actionSendemail()
    {
        $modelCategory = Category::find()->where(['id' => $_POST['category']])->one();
        $from  = "From: <master@mail.ru> \r\n";
        $from .= "Content-type: text/html; charset=utf-8\r\n";
        $adminUser = User::find()->where(['users_type'=>'admin'])->one();

        $admin_email = $adminUser->email;

                $message_to_myemail = '
                    <table>
                     <tbody>
                      <tr>
                       <td>Имя:</td><td>'.$_POST['name'].'</td>
                      </tr>
                      <tr>
                       <td>Телефон:</td><td>'.$_POST['telefon'].'</td>
                      </tr>
                      <tr>
                       <td>E-mail:</td><td>'.$_POST['e_mail'].'</td>
                      </tr>
                      <tr>
                       <td>Категория:</td><td>'.$modelCategory->name.'</td>
                      </tr>
                     </tbody>
                    </table>';

        $status = mail($admin_email, 'ЗАЯВКА НА ПОДБОР СПЕЦИАЛИСТА', $message_to_myemail, $from);
        echo json_encode(["status"=>$status]);
    }

    public function actionVideosearch(){
        $arrayCategory = ArrayHelper::map(Category::find()->all(), 'id', 'name');
        return $this->render('videosearch',[
            'arrayCategory' => $arrayCategory
        ]);
    }

    public function actionAllusercity()
    {
        $modelUsers = Usercontact::find()->all();
        $arrayCity = [];
        foreach($modelUsers as $user){
            if($user['address_city'] != ''){
                $arrayCity[] = $user['address_city'];
            }
        }
        $result = [];
        if($modelUsers != null){
            $result['status'] = 'success';
            $result['city'] = $arrayCity;
        }else{
            $result['city'] = 'error';
        }

        echo json_encode($result);
    }

    public function actionRequestpasswordreset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('passwordResetEmailSend');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('passwordResetEmailNotSend');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    
    public function actionResetpassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionRecomendation()
    {
        return $this->render('recomendation', [
            //'model' => $model,
        ]);
    }
    
    public function actionRecomendationvideoemail()
    {
        $from  = "From: <support@baselik.com> \r\n";
        $from .= "Content-type: text/html; charset=utf-8\r\n";
        $adminUser = User::find()->where(['users_type'=>'admin'])->one();

        $admin_email = $adminUser->email;

                $message_to_myemail = '
                    <table>
                     <tbody>
                      <tr>
                       <td>Имя:</td><td>'.$_POST['name'].'</td>
                      </tr>
                      <tr>
                       <td>Телефон:</td><td>'.$_POST['telefon'].'</td>
                      </tr>
                     </tbody>
                    </table>';

        $status = mail($admin_email, 'Обратный звонок', $message_to_myemail, $from);
        echo json_encode(["status"=>$status]);
    }
    
    public function actionImagetext()
    {



        /*$img = imagecreatefrompng("C:/wamp/www/baselik/web/img/reklam_box_bg1.png");
        $black = imagecolorallocate($img, 255, 255, 255);
        imagestring($img, 15, 40, 160, "Hello world", $black);
        imagestring($img, 5, 40, 175, "Hello world", $black);
        header("Content-Type: image/png");
        imagepng($img);*/

        $userCount = User::find()->count();

        $fontPath1 = 'fonts/BebasNeueRegular.ttf';
        $fontPath2 = 'fonts/ProximaNovaCond-Bold.ttf';
        $fontPath3 = 'fonts/proxima_nova_regular-webfont.ttf';

// Путь к оригинальному изображению
        $imagePath = 'img/reklam_box_bg1.jpg';

// Указываем размер шрифта
        $fontSize1 = 18;
        $fontSize2 = 23;
        $fontSize3 = 14;

// Задаем цвет
        $colorArray = array(226, 226, 226);

// Создаем экземпляр класса LImageHandler
        $ih = new LImageHandler;

// Загружаем изображение
        $imgObj = $ih->load($imagePath);

// Выполняем наложение текста на изображение
        $imgObj->text('C нами уже', $fontPath1, $fontSize1, $colorArray, LImageHandler::CORNER_LEFT_TOP , 40, 150);
        $imgObj->text(number_format($userCount, 0, '.', ' '), $fontPath2, $fontSize2, $colorArray, LImageHandler::CORNER_CENTER_BOX_TOP, 0, 175);
        $imgObj->text('человек', $fontPath3, $fontSize3, $colorArray, LImageHandler::CORNER_LEFT_TOP, 50, 202);

        $imgObj->show(false, 100);

    }

}
