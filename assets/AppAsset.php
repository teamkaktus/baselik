<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "css/bootstrap-theme.css",
        "css/style.css",
        "css/themes/tab.css",
        "css/themes/image.css",
        "css/themes/simple.css",
        "css/labs.css",
        "css/normalize.css",
        "css/jquery.arcticmodal.css",
        "css/themes/dark.css",
        "css/jquery.formstyler.css",
        "css/owl/owl.carousel.css",
        "css/media.css",
        "css/sweetalert.css",
        "fonts/font.css",
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
    ];
    public $js = [
        "js/html5shiv.min.js",
        "js/respond.min.js",
        "js/bootstrap-tooltip.js",
        "js/placeholders.js",
        "js/jquery.formstyler.js",
        "js/jquery.arcticmodal.js",
        "js/owl.carousel.min.js",
        "js/bootstrap-modal.js",
        "js/script.js",
        "js/sweetalert.min.js",
        "js/jquery.cookie.js",
        "js/jquery.session.js",
        "http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&language=ru",
        "js/main.js",
        "js/jquery.scrollUp.min.js",
        
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}