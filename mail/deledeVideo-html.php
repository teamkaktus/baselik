<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="password-reset">
    <h1 style="font-weight: 400; font-size: 30px; color: #484848;">Здравствуйте <?= Html::encode($name) ?>!</h1>

    <p style="color: #484848; font-size: 15px;">Ваше видео видео не прошло модерацию и было удалено с сайта.</p>
    <p style="color: #484848; font-size: 15px;">Причина: "<?=Html::encode($status_description);?>"</p>

</div>
