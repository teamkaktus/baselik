<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="background: #f5f5f5; min-width: 734px;">
<!--<div style="background: url('<?= Yii::$app->urlManager->createAbsoluteUrl(['img/mail/header.png']); ?>') no-repeat; background-size: cover; height: 50px; width: 100%;"></div>-->
<div
    style="background: url('http://baselik.team-kaktus.xyz/img/mail/header.png') no-repeat; background-size: cover; height: 70px; width: 680px; margin: 0 auto; border-radius: 4px 4px 0 0;border-top: 1px solid transparent;">
    <a href="http://baselik.team-kaktus.xyz">
        <div
            style="background: url('http://baselik.team-kaktus.xyz/img/mail/logo.png') no-repeat; width: 244px; height: 49px; margin: 12px 0 0 15px; float: left;"></div>
    </a>
    <div
        style="width: 220px; height: 32px; float: right; margin: 20px 20px 0 0; font-family: verdana,geneva,sans-serif; color: white; font-weight: 500; font-size: 16px; line-height: 18px; text-align: right;">
        <p style="margin: 0;">Сервис видео-портфолио</p><span
            style="font-size: 13px; line-height: 13px;"><?= date("d.m.Y"); ?></span></div>
</div>
<div
    style="width: 720px; margin: 0 auto; background: url('http://baselik.team-kaktus.xyz/img/mail/body-bg.png') center repeat-y;  border-top: 1px solid transparent; font-family: verdana,geneva,sans-serif;">
    <div style="padding: 20px 67px; box-sizing: border-box;">
        <?php $this->beginBody() ?>
        <?= $content ?>
        <?php $this->endBody() ?>
        <hr style="width: 100%; border: 0; border-top: 1px solid #e6e6e6; margin-top: 45px;">
    </div>
</div>
<div
    style="background: url('http://baselik.team-kaktus.xyz/img/mail/footer.png?1') no-repeat; width: 752px; height: 500px; margin: 0 auto;">
    <div
        style="border-top: 1px solid transparent; border-bottom: 1px solid transparent; font-family: verdana,geneva,sans-serif; padding: 10px 67px; box-sizing: border-box; text-align: center;">
        <p style="font-size: 12px; color: #959595; padding: 0 120px; line-height: 20px;">Мы желаем вам успешной работы и
            благодарим Вас за проявленный интерес к услугам нашей компании. Мы работаем для Вас!</p>
        <p style="font-size: 12px; color: #959595; padding: 0 120px;">С уважением, служба поддержки пользователей
            Baselik. </p>
        <p style="font-size: 12px; margin-top: 40px; color: #484848;">Мы в социальных сетях:</p>
        <a href="">
            <div
                style="width: 32px; height: 32px; display: inline-block; background: url('http://baselik.team-kaktus.xyz/img/mail/t.png') no-repeat;"></div>
        </a>
        <a href="">
            <div
                style="width: 32px; height: 32px; display: inline-block; background: url('http://baselik.team-kaktus.xyz/img/mail/f.png') no-repeat;"></div>
        </a>
        <a href="">
            <div
                style="width: 32px; height: 32px; display: inline-block; background: url('http://baselik.team-kaktus.xyz/img/mail/g+.png') no-repeat;"></div>
        </a>
    </div>
</div>
</body>
</html>
<?php $this->endPage() ?>
