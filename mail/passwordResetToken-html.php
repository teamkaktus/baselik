<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resetpassword', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <h1 style="font-weight: 400; font-size: 30px; color: #484848;">Здравствуйте <?= Html::encode($user->username) ?>,</h1>

    <p style="color: #484848; font-size: 15px;">Вы, или кто-то другой запросили восстановление пароля на сайте baselik.ru.</p>

    <p style="color: #484848; font-size: 15px;">Если это были не вы, проигнорируйте это письмо.</p>

    <p style="color: #484848; font-size: 15px;">Если вы подтверждаете этот запрос, перейдите по следующей ссылке:</p>

    <p style="font-size: 15px;"><?= Html::a(Html::encode($resetLink), $resetLink, ['style' => 'color: #2b83de;']) ?></p>

    <p style="color: #484848; font-size: 15px;">Если эта ссылка не работает, откройте новое окно браузера, а затем скопируйте ссылку и вставьте её в адресную строку.</p>

</div>
