<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['allvideo/'.$username]);
?>
<div class="password-reset">
    <h1 style="font-weight: 400; font-size: 30px; color: #484848;">Здравствуйте <?= Html::encode($name) ?>!</h1>

    <p style="color: #484848; font-size: 15px;">Ваше видео успешно прошло модерацию, посмотреть все ваши видео вы можете перейдя по ссылке</p>
    <p style="font-size: 15px;"><?= Html::a(Html::encode($resetLink), $resetLink, ['style' => 'color: #2b83de;']) ?></p>

</div>
